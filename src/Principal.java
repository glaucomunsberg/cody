/**
 * Responsável pelo inicio de todas as operações que inicializam o programa desktop(Cody).
 */
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import system.Cody;
import system.Painel;

import config.Icone;
import config.Config;

public class Principal extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private Icone icones;
	private Config configuracao;
	private Painel paineis;
	private Container container;
	private Cody cody;
	
	public Principal(){
		configuracao = config.Config.getInstancia();
		configuracao.setNovaMensagemNoLog("Sistema Inicializado");
		cody = system.Cody.getInstancia();
		container = this.getContentPane(); 											// Container para os paineis ficarem fixados
		paineis = new Painel();
		icones = new Icone();
		this.setIconImage(icones.getIcone("home").getImage());
		super.setIconImage(icones.getIcone("home").getImage());						//Adicionando "favicon"
		super.setTitle("Cody - Sistema Desktop");									//Adiciona o título do programa
		setJMenuBar( paineis.getPainelMenuBarra() );								//Seta os botões do menu no menu desta divi															//Adiciona o MENU
		GridBagConstraints constraint = new GridBagConstraints();  					//O esquema será de gride sendo o primeiro a cabeça e todos os demais abaixo
        GridBagLayout layout = new GridBagLayout();									//Layout em forma de Gride de uma coluna apenas
        container.setLayout(layout);
        
        
        /**
         * Adiciona cada um dos paineis ao container
         * 	na forma de grid
         */
        constraint.fill = GridBagConstraints.BOTH;
        constraint.gridy = 1;
        constraint.weightx = 1;
        container.add( paineis.getPainelCabecalho(), constraint);
        constraint.gridy = 2;
        constraint.weighty = 0.8;
        container.add(paineis.getPainelHome(), constraint);
        constraint.gridy = 3;
        container.add(paineis.getPainelImportar(), constraint);
        constraint.gridy = 4;
        container.add(paineis.getPainelCriarInstituicao(),constraint);
        constraint.gridy = 5;
        container.add(paineis.getPainelTurmas(), constraint);
        constraint.gridy = 6;
        container.add(paineis.getPainelAlunos(), constraint);
        constraint.gridy = 7;
        container.add(paineis.getPainelTestes(), constraint);
        constraint.gridy = 8;
        container.add(paineis.getPainelTurma(), constraint);
        constraint.gridy = 9;
        container.add(paineis.getPainelAluno(), constraint);
        constraint.gridy = 10;
        container.add(paineis.getPainelExportar(), constraint);
        constraint.gridy = 11;
        container.add(paineis.getPainelTeste(), constraint);
        constraint.gridy = 12;
        container.add(paineis.getPainelAjuda(), constraint);
        
        /**
    	 * Antes de fechar a janela vai tentar salvar as informações
    	 * 	que estão foram modificadas no sistema, caso não
    	 * 	ele simplesmente fecha a janela sem nenhum alerta. Atraves
    	 * 	de um evento
    	 */
        this.addWindowListener(new WindowAdapter()
		{  
	    	
    	    public void windowClosing(WindowEvent e) 
    	    {  
    	    	fechamentoDoPrograma();     
    	    }  
    	});
        
        paineis.jSair.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	fechamentoDoPrograma();
            }
        });
	}
	
	private void fechamentoDoPrograma(){
		int retorno;
    	if(cody.temDadosParaSalvar())
    	{
    		retorno  = configuracao.notificarUsuarioComRetorno("Há dados para salvar, deseja salvar estes dados antes de fechar o programa?") ;
    		 switch(retorno)
    		 {  
    	        case 0:  
    	        	
    	        	if(null == cody.getArquivoAberto()){
    	        		paineis.trocarDePainel("exportar");
    	        	}else{
    	        		boolean gravouComSucesso;
    	        		gravouComSucesso = cody.exportarArquivo(cody.getArquivoAberto());
        				if( gravouComSucesso == false)
        				{
        					JOptionPane.showMessageDialog(null, "Atenção não foi possível salvar as modificações. Tente novamente.");
        				}
        				System.exit(0); 
    	        	}
    	            break;  
    	        case 1: 
		    		System.exit(0);
    	            break;  
    	        case 2:  
    	            break;  
    	        }
    	}
    	else
    	{
    		retorno = configuracao.notificarUsuarioComRetorno("Deseja realmente fechar o programa?") ;
    		switch(retorno)
    		{
    			case 0:
    				System.exit(NORMAL);
    				break;
    			case 1:
    				break;
    		}
    	}    	
	} 
	
	public static void main(String[] args) {
		
		Principal sistema = new Principal();
		java.awt.Toolkit kit = java.awt.Toolkit.getDefaultToolkit();  
		
		//Fecha o programa ao clicar em fechar.
		sistema.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		//Inicia a tela em tamanho maximo.
		sistema.setSize(kit.getScreenSize());
		sistema.setMinimumSize(new java.awt.Dimension(1024, 600));
		 //Torna a janela visivel ao usuário
		sistema.setVisible(true);
		
		//sistema.setSize( sistema.configuracao.getAlturaDaTela() , sistema.configuracao.getLarguraDaTela() ); 		
	}

}
