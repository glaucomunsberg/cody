package system;

import instituicao.Aluno;
import instituicao.JTableModeloTeste;

import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;

import config.Config;
import config.Fonte;
import config.Icone;

/** Painel é a classe que contém todos os paineis de UI usadas no cody. Aqui eles são criado individualmentes e juntados pelo Principal. 
 * Estão aqui presente todo os componentes de cada um dos paineis, bem como seus comportamentos.
 * @author Equipe DSSkywalker.
 */
public class Painel {
	
	private Icone icones;
	private Config config;
	private Fonte fonte;
	private About about;
	private Instituicao instituicao;
	private Dimension tamTela;
	
	private String painelAtivo;
	private Cody cody;
	private JPanel painelCabecalho;
	private JPanel painelHome;
	private JPanel painelImportar;
	private JPanel painelExportar;
	private JPanel painelCriarInstituicao;
	private JPanel painelTurmas;
	private JPanel painelTurma;
	private JPanel painelAlunos;
	private JPanel painelAluno;
	private JPanel painelTestes;
	private JPanel painelTeste;
	private JPanel painelAjuda;
	private JLabel cabecaLabelImagem;
	private JLabel cabecaLabelTexto;
	
	@SuppressWarnings("unused")
	private JButton jButtonExportar;
	private JList jListTurmas;
	private JList jListAlunos;
	private JList jListTeste;
	private JComboBox jComboBoxTipoGenero;

	private final JFormattedTextField jFormattedTextFieldData;
	private JTextField jTextFieldNomeTurma;
	private final JFormattedTextField jTurmaNovaAno;
	private final JFormattedTextField jTurmaAno;
	private JMenuBar jMenuBarra;
	private JFormattedTextField jFormattedTextFieldCPF;
	
	private JTextField jTextFieldNomeAluno;
	private JTextField jTextFieldNomeMaeAluno;
	private JTextField jTextFieldNomePaiAluno;
	private JTextField jTextFieldRG;
	private JTextField jTextFieldEnderecoAluno;
	private JButton jButtonExcluir; //Excluir aluno
	private JTextField jTextFieldNomeAlunoTemp;
	private JTable jTableAptidaoEsportiva;
	private JTable jTableAptidaoSaude;
	private JTable jTableMedidaDeCrescimento;
	
	private JLabel jLabelExportar;
    
	public JMenuItem jSair;
	
	private int alunoIdSelecionado;
	private int turmaIdSelecionado;
	private int testeIdSelecionado;
	private String nomeAlunoSelecionado;
	
	public Painel(){
		
		fonte 					= new Fonte();
		icones 					= new Icone();
		jFormattedTextFieldData = new javax.swing.JFormattedTextField();
		jTurmaNovaAno 			= new javax.swing.JFormattedTextField();
		jTurmaAno 				= new javax.swing.JFormattedTextField();
		jLabelExportar 			= new JLabel();
		config 					= Config.getInstancia();
		cody 					= Cody.getInstancia();
		turmaIdSelecionado 		= -1;
		alunoIdSelecionado 		= -1;
		painelAtivo 			= "home";
		java.awt.Toolkit kit = config.getKit();
		tamTela					= kit.getScreenSize();
		about = new About();
		instituicao = new Instituicao();
		criarCabecalho();
		criarMenuSuperior();
		criarHome();
		criarCabecalho();
		criarImportar();
		criarInstituicao();
		criarTurmas();
		criarAlunos();
		criarTestes();
		criarTeste();
		criarTurma();
		criarAluno();
		criarExportar();
		criarAjuda();
	}
	
	public JMenuBar getPainelMenuBarra(){
		return this.jMenuBarra;
	}
	
	public JPanel getPainelHome(){
		return this.painelHome;
	}
	
	public JPanel getPainelImportar(){
		return this.painelImportar;
	}
	
	public JPanel getPainelCriarInstituicao(){
		return this.painelCriarInstituicao;
	}
	
	public JPanel getPainelTurmas(){
		return this.painelTurmas;
	}
	
	public JPanel getPainelTurma(){
		return this.painelTurma;
	}
	
	public JPanel getPainelAlunos(){
		return this.painelAlunos;
	}
	
	public JPanel getPainelAluno(){
		return this.painelAluno;
	}
	
	public JPanel getPainelTestes(){
		return this.painelTestes;
	}
	
	public JPanel getPainelTeste(){
		return this.painelTeste;
	}
	
	public JPanel getPainelCabecalho(){
		return this.painelCabecalho;
	}
	
	public JPanel getPainelExportar(){
		return this.painelExportar;
	}
	
	public JPanel getPainelAjuda(){
		return this.painelAjuda;
	}
	
	/**
	 * Cria o painelCabelhaço que é o painel que está sempre visivel e ao topo
	 * 	e contem o Icone da ação que está sendo executada e a descrição da ação
	 */
	private void criarCabecalho(){
		
		painelCabecalho = new JPanel();
		GridBagConstraints cons = new GridBagConstraints();				//Diz o tamanho que ocupará no JPanel
		GridBagLayout layout = new GridBagLayout();		
		cabecaLabelImagem = new JLabel( icones.getIcone("home"), SwingConstants.CENTER);
		cabecaLabelTexto = new JLabel("Bem-Vindo ao Projeto ProDown", SwingConstants.CENTER);
		cabecaLabelTexto.setFont(fonte.getFontTitulo());
		
		painelCabecalho.setLayout(layout);							
		
		cons.fill = GridBagConstraints.BOTH;
		cons.weighty = 1;
		cons.weightx = 0.10;
		painelCabecalho.add( cabecaLabelImagem, cons);
		cons.weightx = 0.90;
		painelCabecalho.add(cabecaLabelTexto, cons);
	}

	/**
	 * Cria o menu superior,este é o conjunto de botões que estão
	 * 	disponiveis para o usuário como "ver Turmas" e "ver testes"
	 */
	private void criarMenuSuperior(){
		//      -----------------------------------------------------------------------
		//      | Home  | Turma | Aluno                                               |
		//      -----------------------------------------------------------------------
		JMenuItem 	jAbrir;
		JMenuItem 	jNovo;
	    JMenu 		jMenuArquivo;
	    
	    JMenuItem 	jSalvar;
	    JMenuItem 	jSalvarComo;
	    JMenu 		jMenuHome;
	    JMenuItem 	jMenuIInicio;
	    JMenuItem 	jMenuInstituicao;
	    JMenu 		jAjuda;
	    JMenuItem 	jComoUsar;
	    JMenuItem 	jSobreOPrograma;
	    
		jMenuBarra = new javax.swing.JMenuBar();
        jMenuArquivo = new javax.swing.JMenu();
        jAjuda = new javax.swing.JMenu();
        jAbrir = new javax.swing.JMenuItem();
        jNovo = new javax.swing.JMenuItem();
        jSalvar = new javax.swing.JMenuItem();
        jSalvarComo = new javax.swing.JMenuItem();
        jSair = new javax.swing.JMenuItem();
        jMenuHome = new javax.swing.JMenu();
        jMenuIInicio = new javax.swing.JMenuItem();
        jMenuInstituicao = new javax.swing.JMenuItem();
        jComoUsar = new javax.swing.JMenuItem();
        jSobreOPrograma = new javax.swing.JMenuItem();
        
        jMenuArquivo.setText("Arquivo");
        
        
        jNovo.setText("Criar Instituição");
        jNovo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jNovo.setIcon( icones.getIcone("miniNovo"));
        jNovo.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	trocarDePainel("criarInstituicao");
            }
        });
        jMenuArquivo.add(jNovo);
        
        jAbrir.setText("Abrir...");
        jAbrir.setIcon( icones.getIcone("miniAbrir"));
        jAbrir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jAbrir.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	trocarDePainel("importar");
            }
        });
        jMenuArquivo.add(jAbrir);

        jSalvar.setText("Salvar");
        jSalvar.setIcon( icones.getIcone("miniSalvar"));
        jSalvar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jSalvar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	if( cody.temArquivoAberto() ){
            		if(cody.exportarArquivo(cody.getArquivoAberto())){
            			config.notificaUsuario("Arquivo salvo com sucesso!", "informar");
            		}else{
            			config.notificaUsuario("Erro ao salvar o arquivo", "erro");
            		}
            	}else{
            		trocarDePainel("exportar");
            	}
            }
        });
        jMenuArquivo.add(jSalvar);
        

        jSalvarComo.setText("Salvar como...");
        jSalvarComo.setIcon( icones.getIcone("miniSalvarComo"));
        jSalvarComo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jSalvarComo.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	trocarDePainel("exportar");
            }
        });
        jMenuArquivo.add(jSalvarComo);

        jSair.setText("Sair");
        jSair.setIcon( icones.getIcone("miniSair"));
        jSair.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0));
        jMenuArquivo.add(jSair);

        jMenuBarra.add(jMenuArquivo);
        
        jMenuHome.setText("Visualizar");

        jMenuIInicio.setText("Início");
        jMenuIInicio.setIcon( icones.getIcone("miniInicio"));
        jMenuIInicio.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.CTRL_MASK));
        jMenuIInicio.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	trocarDePainel("home");
            }
        });
        jMenuHome.add(jMenuIInicio);

        jMenuInstituicao.setText("Instituição");
        jMenuInstituicao.setIcon( icones.getIcone("miniInstituicao"));
        jMenuInstituicao.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        jMenuInstituicao.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	trocarDePainel("instituicao");
            }
        });
        jMenuHome.add(jMenuInstituicao);

        jMenuBarra.add(jMenuHome);

        
        jAjuda.setText("Ajuda");

        jComoUsar.setText("Como Usar");
        jComoUsar.setIcon( icones.getIcone("miniComoUsar"));
        jComoUsar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jComoUsar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	trocarDePainel("ajuda");
            }
        });
        jAjuda.add(jComoUsar);

        jSobreOPrograma.setText("Sobre");
        jSobreOPrograma.setIcon( icones.getIcone("miniSobre"));
        jSobreOPrograma.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jSobreOPrograma.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	about.setVisible(true);
            }
        });
        jAjuda.add(jSobreOPrograma);

        jMenuBarra.add(jAjuda);
	}
	
	/**
	 * Cria Home tem a criação do painel Home que contém
	 * 	todas as informações de boas-vindas do sistema
	 */
	private void criarHome(){
		
		//      ---------------------------------------------------------------------
		//      |                                                             	    |
		//		|   ICONE   Texto texto texto             				Criar	    |
		//      |        	texto texto texto							Importar	|
		//		|			texto texto texto							Exportar	|
		//      ---------------------------------------------------------------------
		
		painelHome = new JPanel();
		JLabel jLabelBemVindo;
	    JLabel jLabelNota1;
	    JLabel jLabelNota2;
	    JLabel jLabelNota3;
	    JLabel jLabelNotaText1;
	    JLabel jLabelNotaText2;
	    JLabel jLabelNotaText3;
	    JLabel jLabelNotas;
	    JScrollPane jScrollPane2;
	    JTextArea jTextAreaDescricao;
	    
	    jLabelBemVindo = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaDescricao = new javax.swing.JTextArea();
        jLabelNotas = new javax.swing.JLabel();
        jLabelNota1 = new javax.swing.JLabel();
        jLabelNotaText1 = new javax.swing.JLabel();
        jLabelNota2 = new javax.swing.JLabel();
        jLabelNotaText2 = new javax.swing.JLabel();
        jLabelNota3 = new javax.swing.JLabel();
        jLabelNotaText3 = new javax.swing.JLabel();

        painelHome.setBackground(new java.awt.Color(255, 255, 255));

        jLabelBemVindo.setText("Olá colaborador");
        jLabelBemVindo.setFont( fonte.getFontTitulo() );

        jScrollPane2.setBorder(null);
        jScrollPane2.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao.setEditable(false);
        jTextAreaDescricao.setColumns(20);
        jTextAreaDescricao.setLineWrap(true);
        jTextAreaDescricao.setRows(5);
        jTextAreaDescricao.setText(
        		"\n\n      O formatador de informações do Projeto ProDown ajudará você antes de enviar seus dados para no sistema online. "
        	  + "Aqui no programa desktop você poderá inserir, editar e excluir suas turmas, seus alunos e testes realizados com eles"
        	  + " de forma muito intuitiva. Para começar você precisa criar uma Instituição ou abrir um arquivo manipulado anteriormente.\n"
        	  + "      Você precisa de ajuda? Consulte no menu superir \"Ajuda\" ou visite nosso site http://www.prodown.com.br para obter mais informações e responder suas dúvidas.");
        jTextAreaDescricao.setFont( fonte.getFontTexto() );
        jTextAreaDescricao.setToolTipText("");
        jTextAreaDescricao.setWrapStyleWord(true);
        jTextAreaDescricao.setBorder(null);
        jScrollPane2.setViewportView(jTextAreaDescricao);

        jLabelNotas.setIcon( icones.getIcone("notas") ); // NOI18N

        jLabelNota1.setIcon(icones.getIcone("nota")); // NOI18N

        jLabelNotaText1.setText("Escolha \"Arquivo\" -> \"Nova Instituição\" para criar nova instituição de ensino.");
        jLabelNotaText1.setFont( fonte.getFontTexto() );
        
        jLabelNota2.setIcon( icones.getIcone("nota") ); // NOI18N

        jLabelNotaText2.setText("Escolha \"Arquivo\" -> \"Abrir...\" para abrir um arquivo anteriormente manipulado.");
        jLabelNotaText2.setFont( fonte.getFontTexto() );
        
        jLabelNota3.setIcon( icones.getIcone("nota") ); // NOI18N

        jLabelNotaText3.setText("É necessário o CNPJ da instituição para ser feita a identificação da mesma no ProDown.");
        jLabelNotaText3.setFont( fonte.getFontTexto() );
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(painelHome);
        painelHome.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(jLabelNotas)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2)
                .addGap(29, 29, 29))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(jLabelBemVindo))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNota2)
                                .addGap(18, 18, 18)
                                .addComponent(jLabelNotaText2))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNota1)
                                .addGap(18, 18, 18)
                                .addComponent(jLabelNotaText1))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNota3)
                                .addGap(18, 18, 18)
                                .addComponent(jLabelNotaText3)))))
                .addContainerGap(17, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabelBemVindo, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabelNotas))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelNota1)
                    .addComponent(jLabelNotaText1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelNota2)
                    .addComponent(jLabelNotaText2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelNota3)
                    .addComponent(jLabelNotaText3))
                .addContainerGap(81, Short.MAX_VALUE))
        );
	    
	}
	
	/**
	 * Criar o painel de importação de um arquivo contendo as
	 * 	informações do XML para o sistema
	 */
	private void criarImportar(){
		
		//      ---------------------------------------------------------------------
		//      |                                                             	    |
		//		|   Importar o arquivo Chooser									    |
		//      |        															|
		//		|													Cancelar	Ok	|
		//      ---------------------------------------------------------------------
		
		painelImportar = new JPanel();
		painelImportar.setVisible(false);
		
		final JFileChooser jFileChooser;
	    JLabel jLabelImportar;
	    jLabelImportar = new javax.swing.JLabel();
        jFileChooser = new javax.swing.JFileChooser();

        painelImportar.setBackground(new java.awt.Color(255, 255, 255));

        jLabelImportar.setText("Escolha um arquivo");
        jLabelImportar.setFont(fonte.getFontTitulo());

        ExtensionFileFilter codyFilter = new ExtensionFileFilter(new String("cody"), "Cody Files");
        jFileChooser.setAcceptAllFileFilterUsed(false);
        jFileChooser.addChoosableFileFilter( codyFilter);
        jFileChooser.setApproveButtonText("Abrir");
        jFileChooser.setBackground(new java.awt.Color(254, 254, 254));
        jFileChooser.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	String botaoPressionado = evt.getActionCommand();
            	 File f = jFileChooser.getSelectedFile();
            	 if(f != null && botaoPressionado.equals("ApproveSelection")){
            		 /**
            		  * Importa o arquivo
            		  */
            		 if(cody.importarArquivo(f.getAbsolutePath())){
            			 config.notificaUsuario("O arquivo foi carregado com sucesso!", "avisar");
            			 trocarDePainel("home");
            		 }else{
            			 trocarDePainel("home");
            		 }
 
            	 }else{
            		 trocarDePainel("home");
            	 }
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(painelImportar);
        painelImportar.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelImportar, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jFileChooser, javax.swing.GroupLayout.DEFAULT_SIZE, this.tamTela.width, Short.MAX_VALUE)
                        .addGap(21, 21, 21))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabelImportar, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jFileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, this.tamTela.height - 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
        );
	}

	/**
	 * Cria o painel para a instituição,lembre-se que esta está relacinada
	 * 	apenas a criação da instituição e não está para editar.
	 */
	
	private void criarInstituicao(){
		//      ---------------------------------------------------------------------
		//      |                                                             	    |
		//		|   Criar Instituicao											    |
		//      |     Nome   														|
		//      |     Tipo   														|
		//      |     CNPJ   														|
		//		|													Cancelar	Ok	|
		//      ---------------------------------------------------------------------
		
		painelCriarInstituicao = new JPanel();
		painelCriarInstituicao.setVisible(false);
		
		JButton jButtonSalvar;
	    JButton jButtonVoltar;
		final JComboBox jComboBoxTipoInstituicao;
	    final JFormattedTextField jFormattedTextFieldCNPJ;
	    JLabel jLabelCNPJ;
	    JLabel jLabelCriarInstuicao;
	    JLabel jLabelIconeInfo;
	    final JLabel jLabelNomeInstituicao;
	    final JLabel jLabelTipoInstituicao;
	    JScrollPane jScrollPane2;
	    JTextArea jTextAreaDescricao;
	    final JTextField jTextFieldNomeInstuicao;
	    
	    jLabelCriarInstuicao = new JLabel();
        jScrollPane2 = new JScrollPane();
        jTextAreaDescricao = new JTextArea();
        jLabelIconeInfo = new JLabel();
        jLabelNomeInstituicao = new JLabel();
        jTextFieldNomeInstuicao = new JTextField();
        jLabelTipoInstituicao = new JLabel();
        jComboBoxTipoInstituicao = new JComboBox();
        jLabelCNPJ = new JLabel();
        jFormattedTextFieldCNPJ = new JFormattedTextField();
        jButtonVoltar = new JButton();
        jButtonSalvar = new JButton();

        painelCriarInstituicao.setBackground(new java.awt.Color(255, 255, 255));

        jLabelCriarInstuicao.setText("Criar Instituição");
        jLabelCriarInstuicao.setFont(fonte.getFontTitulo());
        
        jScrollPane2.setBorder(null);
        jScrollPane2.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao.setEditable(false);
        jTextAreaDescricao.setColumns(20);
        jTextAreaDescricao.setLineWrap(true);
        jTextAreaDescricao.setRows(5);
        jTextAreaDescricao.setText("      Para começar é necessário que você identifique sua instituição de ensino, basta ter em mãos o Nome da Instituição, CNPJ e o Tipo de Ensino (Especial ou Regular).\n      Essas informações são fundamentais e não é possível modificá-las posteriormente. Consulte o Ajuda para maiores informações.\n\n      *Campos Obrigatórios");
        jTextAreaDescricao.setFont( fonte.getFontTexto() );
        jTextAreaDescricao.setToolTipText("");
        jTextAreaDescricao.setWrapStyleWord(true);
        jTextAreaDescricao.setBorder(null);
        jScrollPane2.setViewportView(jTextAreaDescricao);
        jTextFieldNomeInstuicao.setToolTipText("Insira o nome completo da sua Instituição:");
        
        jLabelIconeInfo.setIcon( icones.getIcone("info") ); // NOI18N

        jLabelNomeInstituicao.setText( "*Nome da Instituição:" );
        jLabelNomeInstituicao.setFont( fonte.getFontTexto() );
        
        jLabelTipoInstituicao.setText( "*Tipo de Instituição:" );
        jLabelTipoInstituicao.setFont( fonte.getFontTexto() );
        
        jComboBoxTipoInstituicao.setModel(new DefaultComboBoxModel(new String[] { "Ensino Especial", "Ensino Regular" }));
        jComboBoxTipoInstituicao.setToolTipText("Escolha \"Especial\" se a sua escola é dedicada unicamente a portadores da Síndrome de Down.");
        jComboBoxTipoInstituicao.setFont( fonte.getFontTexto() );
        
        jLabelCNPJ.setText( "*CNPJ:" );
        jLabelCNPJ.setFont( fonte.getFontTexto() );

        try {
            jFormattedTextFieldCNPJ.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedTextFieldCNPJ.setToolTipText("Cada instituição, seja ela pública ou privada, possui um CNPJ. Digite-o aqui.");
        jFormattedTextFieldCNPJ.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                
            }
        });

        jButtonVoltar.setText("Voltar");
        jButtonVoltar.setFont(fonte.getFontTexto());
        jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	trocarDePainel("home");
            }
        });
        
        /**
         * Salvar implica em adicionar e chamar o exportar
         */
        jButtonSalvar.setText("Salvar");
        jButtonSalvar.setFont(fonte.getFontTexto());
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	if("".equals( jTextFieldNomeInstuicao.getText() ) || " ".equals( jTextFieldNomeInstuicao.getText() ) ){
            		config.notificaUsuario("Preencha o Nome da Instituição e o CNPJ corretamente antes de salvar.", "avisar");
            	}else{
            		
            		if( cody.validaCNPJ(jFormattedTextFieldCNPJ.getText() ) ){
            			cody.setNomeInstituicao( jTextFieldNomeInstuicao.getText() );
                		cody.setCnpjInstituicao( jFormattedTextFieldCNPJ.getText() );
                		cody.setTipoInstituicao( jComboBoxTipoInstituicao.getSelectedItem().toString() );
                		int retorno;
                		retorno =config.notificarUsuarioComRetorno("Deseja criar um arquivo com as informações atualmente inseridas?");
                		if(retorno == 0){
                			trocarDePainel("exportar");
                		}else{
                			trocarDePainel("home");
                		}
                    	
            		}else{
            			config.notificaUsuario("CNPJ inválido! Para salvar é necessário um CNPJ válido.", "avisar");
            		}
            		
            	}
            }
        });
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(painelCriarInstituicao);
        painelCriarInstituicao.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jComboBoxTipoInstituicao, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jFormattedTextFieldCNPJ)
                        .addGap(2, 2, 2))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelCNPJ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(211, 211, 211))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelTipoInstituicao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(125, 125, 125))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelNomeInstituicao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(116, 116, 116))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelCriarInstuicao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(145, 145, 145))
                    .addComponent(jTextFieldNomeInstuicao))
                .addGap(28, 28, 28)
                .addComponent(jLabelIconeInfo)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButtonVoltar, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                        .addGap(13, 13, 13)
                        .addComponent(jButtonSalvar, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)))
                .addGap(23, 23, 23))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabelCriarInstuicao, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNomeInstituicao)
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldNomeInstuicao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabelIconeInfo))
                        .addGap(18, 18, 18)
                        .addComponent(jLabelTipoInstituicao)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBoxTipoInstituicao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabelCNPJ)
                        .addGap(18, 18, 18)
                        .addComponent(jFormattedTextFieldCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
	}
	
	/**
	 * Cria o painel que dá suporte a manipulação das turmas
	 */
	
	public void criarTurmas(){
		painelTurmas = new JPanel();
		painelTurmas.setVisible(false);
		
		JButton jButtonSalvar;
	    JLabel jLabelCriarInstuicao;
	    JLabel jLabelIconeInfo;
	    JLabel jLabelNomeTurma;
	    JLabel jLabelTipoInstituicao;
	   
	    JScrollPane jScrollPane1;
	    JScrollPane jScrollPane2;
	    JTextArea jTextAreaDescricao;
	    final JTextField jTextFieldNomeTurma;
	    
	    jLabelCriarInstuicao = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaDescricao = new javax.swing.JTextArea();
        jLabelIconeInfo = new javax.swing.JLabel();
        jLabelNomeTurma = new javax.swing.JLabel();
        jTextFieldNomeTurma = new javax.swing.JTextField();
        jLabelTipoInstituicao = new javax.swing.JLabel();
        jButtonSalvar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListTurmas = new javax.swing.JList( new String[] {""} );
        
        try {
            jTurmaNovaAno.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTurmaNovaAno.setToolTipText("");
        

        painelTurmas.setBackground(new java.awt.Color(255, 255, 255));

        jLabelCriarInstuicao.setText("Turmas");
        jLabelCriarInstuicao.setFont( fonte.getFontTitulo() );

        jScrollPane2.setBorder(null);
        jScrollPane2.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao.setEditable(false);
        jTextAreaDescricao.setColumns(20);
        jTextAreaDescricao.setLineWrap(true);
        jTextAreaDescricao.setRows(5);
        jTextAreaDescricao.setText("      Crie turmas ou selecione uma turma na lista. Logo após você poderá inserir, editar e remover os alunos e testes vinculados.");
        jTextAreaDescricao.setFont( fonte.getFontTexto() );
        jTextAreaDescricao.setToolTipText("");
        jTextAreaDescricao.setWrapStyleWord(true);
        jTextAreaDescricao.setBorder(null);
        jScrollPane2.setViewportView(jTextAreaDescricao);
        
        jTextFieldNomeTurma.setToolTipText("Digite aqui o código ou o nome da turma");
        jTurmaNovaAno.setToolTipText("Digite aqui o ano de vigência da turma.");

        jLabelIconeInfo.setIcon( icones.getIcone("info") ); // NOI18N

        jLabelNomeTurma.setText("Criar Turma");
        jLabelNomeTurma.setFont( fonte.getFontTexto() );
        
        jLabelTipoInstituicao.setText("Lista de Turmas");
        jLabelTipoInstituicao.setFont( fonte.getFontTexto() );
        
        jButtonSalvar.setText("Salvar");
        jButtonSalvar.setFont( fonte.getFontTexto() );
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	if( ("".equals(jTextFieldNomeTurma.getText())  || " ".equals(jTextFieldNomeTurma.getText()) || "    ".equals(jTurmaNovaAno.getText()) ) || Integer.parseInt(jTurmaNovaAno.getText() ) < 2000 ){
            		config.notificaUsuario("A turma deve conter um Código/Nome e Ano válido.", "avisar");
            	}else{
            		if( cody.setNovaTurma(jTextFieldNomeTurma.getText(), jTurmaNovaAno.getText() ) ){
            			config.notificaUsuario("Turma inserida com sucesso!", "informar");
            			jListTurmas.setListData( cody.getListaTurmas() );
            			
            		}else{
            			config.notificaUsuario("Não foi possível inserir esta turma. Verifique o nome/ano e tente novamente.", "erro");
            		}
            		jTextFieldNomeTurma.setText("");
            		jTurmaNovaAno.setText("");
            	}
            }
        });

        jScrollPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jListTurmas.setListData( cody.getListaTurmas() );
        
        jListTurmas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jListTurmas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jListTurmas.setFont( fonte.getFontTexto() );
        jListTurmas.setDragEnabled(true);
        jListTurmas.addListSelectionListener(
				new ListSelectionListener()
				{
					@Override
					public void valueChanged( ListSelectionEvent event)
					{
						if( jListTurmas.getSelectedIndex() >= 0)
						{
							escondePainelAtivo();
							turmaIdSelecionado = jListTurmas.getSelectedIndex();
							trocarDePainel("turma");
						}
					}
				}
				
		);
        jScrollPane1.setViewportView(jListTurmas);
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(painelTurmas);
        painelTurmas.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(44, 44, 44)
                            .addComponent(jScrollPane1))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(33, 33, 33)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addComponent(jLabelTipoInstituicao, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                                            .addGap(155, 155, 155))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addComponent(jLabelNomeTurma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addGap(122, 122, 122))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(jTextFieldNomeTurma)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addGap(0, 0, Short.MAX_VALUE)
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jTurmaNovaAno, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                    .addComponent(jLabelIconeInfo)
                                    .addGap(12, 12, 12))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabelCriarInstuicao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGap(211, 211, 211)))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGap(23, 23, 23))
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(jLabelCriarInstuicao, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabelNomeTurma)
                            .addGap(18, 18, 18)
                            .addComponent(jTextFieldNomeTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTurmaNovaAno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jLabelIconeInfo))
                    .addGap(18, 18, 18)
                    .addComponent(jButtonSalvar)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jLabelTipoInstituicao)
                    .addGap(18, 18, 18)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(20, Short.MAX_VALUE))
            );
	}

	/**
	 * Cria o painel que dá suporte a uma turma específica, assim
	 * 	sendo possível ver e editar essa turma, bem como ver os alunos e 	
	 * 	testes realizados
	 */
	public void criarTurma(){
		this.painelTurma = new JPanel();
		this.painelTurma.setVisible(false);
		
		JButton jButtonAlunosTurma;
	    JButton jButtonSalvar;
	    JButton jButtonTestesTurma;
	    JButton jButtonVoltar2;
	    JButton jButtonExcluir;
	    JLabel jLabelIconeInfo;
	    
	    JLabel jLabelTurma;
	    JScrollPane jScrollPane2;
	    JTextArea jTextAreaDescricao;
	    
	    
	    jLabelTurma = new JLabel();
        jScrollPane2 = new JScrollPane();
        jTextAreaDescricao = new JTextArea();
        jLabelIconeInfo = new JLabel();
       
        jTextFieldNomeTurma = new JTextField();
        jButtonAlunosTurma = new JButton();
        jButtonSalvar = new JButton();
        jButtonTestesTurma = new JButton();
        jButtonVoltar2 = new JButton();
        jButtonExcluir = new JButton();
        
        painelTurma.setBackground(new java.awt.Color(255, 255, 255));

        jLabelTurma.setText("Turma");
        jLabelTurma.setFont(fonte.getFontTitulo());

        jScrollPane2.setBorder(null);
        jScrollPane2.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao.setEditable(false);
        jTextAreaDescricao.setColumns(20);
        jTextAreaDescricao.setLineWrap(true);
        jTextAreaDescricao.setRows(5);
        jTextAreaDescricao.setText("      Aqui há os alunos e testes referentes a turma selecionada. Para inserir, editar ou excluir basta selecionar Alunos da Turma ou Testes da Turma.");
        jTextAreaDescricao.setFont(fonte.getFontTexto());
        jTextAreaDescricao.setToolTipText("");
        jTextAreaDescricao.setWrapStyleWord(true);	
        jTextAreaDescricao.setBorder(null);
        jScrollPane2.setViewportView(jTextAreaDescricao);

        jLabelIconeInfo.setIcon(this.icones.getIcone("info"));
        
        jButtonAlunosTurma.setText("Alunos da Turma");
        jButtonAlunosTurma.setFont(fonte.getFontTexto());
        jButtonAlunosTurma.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	escondePainelAtivo();
            	trocarDePainel("alunos");
            }
        });

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.setFont(fonte.getFontTexto());
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	if( ("".equals(jTextFieldNomeTurma.getText())  || " ".equals(jTextFieldNomeTurma.getText()) || "    ".equals(jTurmaAno.getText()) ) || Integer.parseInt(jTurmaAno.getText() ) < 2000  ){
            		config.notificaUsuario("Insira um Código/Nome e Ano válido na turma antes de salvar.", "avisar");
            	}else{
            		if( config.notificarUsuarioComRetorno("Você deseja realmente modificar dessa turma?") == 0 ){
                		if( cody.setEditarTurmaById( turmaIdSelecionado, jTextFieldNomeTurma.getText(), jTurmaAno.getText()) ){
                			config.notificaUsuario("Turma modificado com sucesso!", "informar");
                			escondePainelAtivo();
                			trocarDePainel("turmas");
                		}else{
                			config.notificaUsuario("Não foi possível modificar esta turma.", "erro");
                		}
                	}
            	}
            	
            }
        });
        
        jTextFieldNomeTurma.setToolTipText("Nome da turma");
        jTurmaAno.setToolTipText("Ano de vigência da turma");
        
        jButtonTestesTurma.setText("Avaliações da Turma");
        jButtonTestesTurma.setFont(fonte.getFontTexto());
        jButtonTestesTurma.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	escondePainelAtivo();
            	trocarDePainel("testes");
            }
        });

        jButtonVoltar2.setText("Voltar");
        jButtonVoltar2.setFont(fonte.getFontTexto());
        jButtonVoltar2.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	trocarDePainel("turmas");
            }
        });
        
        jButtonExcluir.setText("Excluir");
        jButtonExcluir.setFont(fonte.getFontTexto());
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	if( config.notificarUsuarioComRetorno("Você deseja realmente remover essa turma?") == 0 ){
            		if( cody.removerTurmaById( turmaIdSelecionado) ){
            			config.notificaUsuario("Turma removida com sucesso!", "informar");
            			escondePainelAtivo();
            			trocarDePainel("turmas");
            		}else{
            			config.notificaUsuario("Não foi possível remover esta turma.", "erro");
            		}
            	}
            }
        });
        
        try {
            jTurmaNovaAno.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(painelTurma);
        painelTurma.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(18, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldNomeTurma)
                            .addComponent(jButtonAlunosTurma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonTestesTurma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 39, Short.MAX_VALUE)
                                .addComponent(jButtonExcluir)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTurmaAno, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabelIconeInfo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButtonVoltar2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelTurma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabelTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelIconeInfo)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTextFieldNomeTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTurmaAno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonSalvar)
                            .addComponent(jButtonExcluir))
                        .addGap(35, 35, 35)
                        .addComponent(jButtonAlunosTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonTestesTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonVoltar2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );
		
	}
	
	/**
	 * Cria o painel que pode-se ver todos os alunos de cada uma das turmas
	 * 	cadastradas no sistema cody
	 */
	
	public void criarAlunos(){
		this.painelAlunos = new JPanel();
		painelAlunos.setVisible(false);
		
		JButton jButtonSalvar;
	    JButton jButtonVoltar;
	    JLabel jLabelCriarAluno;
	    JLabel jLabelIconeInfo;
	    JLabel jLabelNomeAluno;
	    JLabel jLabelTipoAluno;
	   
	    JScrollPane jScrollPane1;
	    JScrollPane jScrollPane2;
	    JTextArea jTextAreaDescricao;
	    
	    jLabelCriarAluno = new JLabel();
        jScrollPane2 = new JScrollPane();
        jTextAreaDescricao = new JTextArea();
        jLabelIconeInfo = new JLabel();
        jLabelNomeAluno = new JLabel();
        jTextFieldNomeAlunoTemp = new JTextField();
        jLabelTipoAluno = new JLabel();
        jButtonVoltar = new JButton();
        jButtonSalvar = new JButton();
        jScrollPane1 = new JScrollPane();
        jListAlunos = new JList();

        painelAlunos.setBackground(new java.awt.Color(255, 255, 255));

        jLabelCriarAluno.setText("Alunos");
        jLabelCriarAluno.setFont(fonte.getFontTitulo());

        jScrollPane2.setBorder(null);
        jScrollPane2.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao.setEditable(false);
        jTextAreaDescricao.setColumns(20);
        jTextAreaDescricao.setLineWrap(true);
        jTextAreaDescricao.setRows(5);
        jTextAreaDescricao.setText("          Aqui você pode inserir um aluno. Digite o nome e clique em \"criar\" para cadastrar as demais informações necessárias. Através da lista apresentada você poderá editar ou excluir um aluno desta turma.");
        jTextAreaDescricao.setFont(fonte.getFontTexto());
        jTextAreaDescricao.setToolTipText("");
        jTextAreaDescricao.setWrapStyleWord(true);
        jTextAreaDescricao.setBorder(null);
        jScrollPane2.setViewportView(jTextAreaDescricao);

        jLabelIconeInfo.setIcon( icones.getIcone("info") ); // NOI18N
        
        jLabelNomeAluno.setText("Criar Aluno");
        jLabelNomeAluno.setFont(fonte.getFontTexto());
        
        jLabelTipoAluno.setText("Lista de Alunos");
        jLabelTipoAluno.setFont(fonte.getFontTexto());
        
        
        jButtonVoltar.setText("Voltar");
        jButtonVoltar.setFont(fonte.getFontTexto());
        jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	escondePainelAtivo();
            	trocarDePainel("turma");
            }
        });
        
        jButtonSalvar.setText("Criar");
        jButtonSalvar.setFont(fonte.getFontTexto());
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	nomeAlunoSelecionado = jTextFieldNomeAlunoTemp.getText();
        		alunoIdSelecionado = -1;
        		escondePainelAtivo();
        		trocarDePainel("aluno");
            }
        });
        
        jScrollPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        /**
         * Lista de Alunos
         */
        jListAlunos.setFont(fonte.getFontTexto());
        jListAlunos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jListAlunos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jListAlunos.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged( ListSelectionEvent event)
			{
				if( jListAlunos.getSelectedIndex() >= 0)
				{
					escondePainelAtivo();
					alunoIdSelecionado = jListAlunos.getSelectedIndex();
					trocarDePainel("aluno");
				}
			}
		});
        
        jScrollPane1.setViewportView(jListAlunos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(painelAlunos);
        painelAlunos.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelTipoAluno, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                                .addGap(149, 149, 149))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNomeAluno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(116, 116, 116))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelCriarAluno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(145, 145, 145))
                            .addComponent(jTextFieldNomeAlunoTemp))))
                .addGap(28, 28, 28)
                .addComponent(jLabelIconeInfo)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(23, 23, 23))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabelCriarAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane2)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNomeAluno)
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldNomeAlunoTemp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabelIconeInfo))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonSalvar)
                        .addGap(17, 17, 17)
                        .addComponent(jLabelTipoAluno)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
	}
	
	/**
	 * Aqui está o aluno, onde pode-se editar ou criar um
	 */
	public void criarAluno(){
		this.painelAluno = new JPanel();
		this.painelAluno.setVisible(false);
		
		
	    JButton jButtonSalvar;
	    JButton jButtonVoltar;
	    
	    JLabel jLabelAluno;
	    JLabel jLabelCPF;
	    JLabel jLabelIdadeAluno;
	    JLabel jLabelNomeAluno;
	    JLabel jLabelNomeMaeAluno;
	    JLabel jLabelNomePaiAluno;
	    JLabel jLabelRG;
	    JLabel jLabelTipoGenero;
	    JLabel jLabelEnderecoAluno;
	    
	    jLabelAluno = new javax.swing.JLabel();
	    jLabelEnderecoAluno = new javax.swing.JLabel();
        jLabelCPF = new javax.swing.JLabel();
        jLabelNomeAluno = new javax.swing.JLabel();
        jTextFieldNomeAluno = new javax.swing.JTextField();
        jLabelTipoGenero = new javax.swing.JLabel();
        jComboBoxTipoGenero = new javax.swing.JComboBox(new String[] {"Masculino", "Feminino"});
        jFormattedTextFieldCPF = new javax.swing.JFormattedTextField();
        jButtonVoltar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jLabelRG = new javax.swing.JLabel();
        
        java.text.NumberFormat f = java.text.NumberFormat.getNumberInstance(); 
    	f.setMaximumIntegerDigits(20);
        
        jTextFieldRG = new javax.swing.JTextField();
        jLabelIdadeAluno = new javax.swing.JLabel();
        jLabelNomeMaeAluno = new javax.swing.JLabel();
        jTextFieldNomeMaeAluno = new javax.swing.JTextField();
        jLabelNomePaiAluno = new javax.swing.JLabel();
        jTextFieldEnderecoAluno = new javax.swing.JTextField();
        jTextFieldNomePaiAluno = new javax.swing.JTextField();
        jButtonExcluir = new javax.swing.JButton();
        

        painelAluno.setBackground(new java.awt.Color(255, 255, 255));

        jLabelAluno.setText("Aluno");
        jLabelAluno.setFont( fonte.getFontTitulo() );

        jLabelCPF.setText("CPF");
        jLabelCPF.setFont( fonte.getFontTexto() );
        
        //jTextFieldRG.setDocument();
        
        jLabelNomeAluno.setText("*Nome");
        jLabelNomeAluno.setFont( fonte.getFontTexto() );

        jLabelTipoGenero.setText("Gênero");
        jLabelTipoGenero.setFont( fonte.getFontTexto() );
        
        jLabelEnderecoAluno.setText("Endereço");
        jLabelEnderecoAluno.setFont( fonte.getFontTexto() );

        //jComboBoxTipoGenero.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Masculino", "Feminino" }));
        jComboBoxTipoGenero.setFont( fonte.getFontTexto() );

        try {
            jFormattedTextFieldCPF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            
        }
        
                
        jFormattedTextFieldCPF.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
               
            }
        });

        jButtonVoltar.setText("Voltar");
        jButtonVoltar.setFont( fonte.getFontTexto() );
        jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	trocarDePainel("alunos");
            }
        });
        
        jButtonSalvar.setText("Salvar");
        jButtonSalvar.setFont( fonte.getFontTexto() );
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	if(!"".equals(jTextFieldNomeAluno.getText()) && !"".equals(jTextFieldNomeMaeAluno.getText()) ){
            	  if ( cody.validaRG(jTextFieldRG.getText())){
            		if(jFormattedTextFieldData.getText() != null && cody.validaDataNasc( jFormattedTextFieldData.getText() ) ){
                		boolean retornoID = false;
                    	if(alunoIdSelecionado == -1){
                    		retornoID = cody.setNovoAluno(turmaIdSelecionado, jTextFieldNomeAluno.getText(), jFormattedTextFieldData.getText(), jTextFieldNomeMaeAluno.getText(), jTextFieldNomePaiAluno.getText(), jComboBoxTipoGenero.getSelectedItem().toString(), jFormattedTextFieldCPF.getText(), jTextFieldRG.getText(),jTextFieldEnderecoAluno.getText() );
                    	}else{
                    		if(alunoIdSelecionado >= 0){
                    			retornoID = cody.setEditarAluno(turmaIdSelecionado, alunoIdSelecionado, jTextFieldNomeAluno.getText(), jFormattedTextFieldData.getText(), jTextFieldNomeMaeAluno.getText(),  jTextFieldNomePaiAluno.getText(), jComboBoxTipoGenero.getSelectedItem().toString(), jFormattedTextFieldCPF.getText(), jTextFieldRG.getText(), jTextFieldEnderecoAluno.getText());
                    		}else{
                    			config.notificaUsuario("Ooops! Houve um erro em relação ao aluno. Desculpe-me :/", "erro");
                    			trocarDePainel("home");
                    		}
                    	}
                    	if(retornoID){
                    		alunoIdSelecionado = -1;
                    		config.notificaUsuario("Aluno inserido com sucesso!", "informar");
                    		trocarDePainel("alunos");
                    	}else{
                    		config.notificaUsuario("Aluno não foi inserido. Verifique os dados e tente novamente .", "erro");
                    	}
                	}else{
                		config.notificaUsuario("A faixa-etária do aluno deve ser entre 10 e 20 anos.", "avisar");
                	}
            	  }
            	  else{
            		  config.notificaUsuario("RG não pode haver caracteres especiais ou letras, quantidade de números máximo: 20", "avisar");
            	  }
            	}else{
            		config.notificaUsuario("Complete os campos Obrigatórios: Nome, Nome da Mãe e Data de Nascimento.", "avisar");
            	}
            }
        });
        
        jTextFieldNomeAluno.setToolTipText("Digite o nome completo do aluno.");
        jFormattedTextFieldData.setToolTipText("Digite a data de Nascimento do aluno.");
        jTextFieldNomeMaeAluno.setToolTipText("Digite o nome completo da mãe do aluno.");
        jTextFieldNomePaiAluno.setToolTipText("Digite o nome completo da pai do aluno, se houver.");
        jFormattedTextFieldCPF.setToolTipText("Digite o CPF do aluno, se houver.");
        jTextFieldRG.setToolTipText("Digite o Registro Geral do aluno, se houver.");
        jTextFieldEnderecoAluno.setToolTipText("Digite o Endereço do aluno, se houver. Por exemplo: Rua Talliano N:08, São Paulo, SP.");
        
        jLabelRG.setText("RG");
        jLabelRG.setFont( fonte.getFontTexto() );

        jLabelIdadeAluno.setText("*Data de Nascimento");
        jLabelIdadeAluno.setFont( fonte.getFontTexto() );

        jLabelNomeMaeAluno.setText("*Nome da Mãe");
        jLabelNomeMaeAluno.setFont( fonte.getFontTexto() );

        jLabelNomePaiAluno.setText("Nome do Pai");
        jLabelNomePaiAluno.setFont( fonte.getFontTexto() );

        jButtonExcluir.setText("Excluir");
        jButtonExcluir.setFont( fonte.getFontTexto() );
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	int retorno;
            	if( jButtonExcluir.isEnabled() ){
            		retorno = config.notificarUsuarioComRetorno("Você tem certeza que deseja excluir este Aluno?");
            		if(retorno == 0){
            			if( cody.removeAlunoByTurma(turmaIdSelecionado,alunoIdSelecionado )){
                			config.notificaUsuario("Aluno removido com sucesso!", "informar");
                			trocarDePainel("alunos");
                		}else{
                			config.notificaUsuario("Houve um erro ao excluir o Aluno. Tente novamente.", "erro");
                		}
            		}
            	}
            }
        });

        try {
            jFormattedTextFieldData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedTextFieldData.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
               
            }

        });
        painelAluno.setAutoscrolls(true);
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(painelAluno);
        painelAluno.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(24, 24, 24))
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabelAluno, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE)
                            .addGap(497, 497, 497))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jTextFieldNomeMaeAluno)
                                            .addGap(91, 91, 91))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabelEnderecoAluno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabelNomePaiAluno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jTextFieldNomePaiAluno)
                                                .addComponent(jTextFieldEnderecoAluno))
                                            .addGap(91, 91, 91)))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelCPF, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldRG, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jFormattedTextFieldCPF, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelIdadeAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jFormattedTextFieldData, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelNomeAluno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabelNomeMaeAluno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jTextFieldNomeAluno)
                                            .addGap(79, 79, 79)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelRG, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelTipoGenero, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxTipoGenero, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGap(100, 100, 100))))
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(jLabelAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelNomeAluno)
                        .addComponent(jLabelTipoGenero))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldNomeAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jComboBoxTipoGenero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelNomeMaeAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabelRG))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldNomeMaeAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldRG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(12, 12, 12)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelCPF)
                        .addComponent(jLabelNomePaiAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldNomePaiAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jFormattedTextFieldCPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(24, 24, 24)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelEnderecoAluno)
                        .addComponent(jLabelIdadeAluno))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldEnderecoAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jFormattedTextFieldData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(36, 36, 36)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(164, 164, 164))
            );
	}
        
        
	/**
	 * Aqui cria-se o painel que mostra os testes de cada turma, veja que os testes
	 * 	são da turma e não do aluno, na exibição
	 */
	
	public void criarTestes(){
		this.painelTestes = new JPanel();
		painelTestes.setVisible(false);
		JButton jButtonSalvar;
	    JButton jButtonVoltar;
	    final JFormattedTextField jFormattedTextTeste;
	    JLabel jLabelCriarTeste;
	    JLabel jLabelIconeInfo;
	    JLabel jLabelNomeTeste;
	    JLabel jLabelTipoTeste;
	    JScrollPane jScrollPane1;
	    JScrollPane jScrollPane2;
	    JTextArea jTextAreaDescricao;
	    final JTextField jTextFieldNomeTeste;
	    
	    jLabelCriarTeste = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaDescricao = new javax.swing.JTextArea();
        jLabelIconeInfo = new javax.swing.JLabel();
        jLabelNomeTeste = new javax.swing.JLabel();
        jTextFieldNomeTeste = new javax.swing.JTextField();
        jLabelTipoTeste = new javax.swing.JLabel();
        jButtonVoltar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListTeste = new javax.swing.JList();
        jFormattedTextTeste = new javax.swing.JFormattedTextField();

        painelTestes.setBackground(new java.awt.Color(255, 255, 255));

        jLabelCriarTeste.setText("Avaliações");
        jLabelCriarTeste.setFont(fonte.getFontTitulo());
        
        jScrollPane2.setBorder(null);
        jScrollPane2.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao.setEditable(false);
        jTextAreaDescricao.setColumns(20);
        jTextAreaDescricao.setLineWrap(true);
        jTextAreaDescricao.setRows(5);
        jTextAreaDescricao.setText("      Para cada avaliação é necessário informar um Código/Nome e a Data de ocorrência. Através da lista de avaliações, escolha uma avaliação para adicionar, editar ou excluir as informações dos alunos que participaram do teste.\n      Note que não precisa ter todos os alunos para digitar os testes do que já estão listados.");
        jTextAreaDescricao.setFont( fonte.getFontTexto() );
        jTextAreaDescricao.setToolTipText("");
        jTextAreaDescricao.setWrapStyleWord(true);
        jTextAreaDescricao.setBorder(null);
        jScrollPane2.setViewportView(jTextAreaDescricao);

        jLabelIconeInfo.setIcon( icones.getIcone("info")); // NOI18N

        jLabelNomeTeste.setText("Criar Avaliação");
        jLabelNomeTeste.setFont( fonte.getFontTexto() );
        
        jLabelTipoTeste.setText("Lista de Avaliações");
        jLabelTipoTeste.setFont( fonte.getFontTexto() );
        
        jButtonVoltar.setText("Voltar");
        jButtonVoltar.setFont( fonte.getFontTexto() );
        jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	escondePainelAtivo();
            	trocarDePainel("turma");
            }
        });
        
        jButtonSalvar.setText("Criar");
        jButtonSalvar.setFont( fonte.getFontTexto() );
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	if( "".equals( jTextFieldNomeTeste.getText() ) || " ".equals( jTextFieldNomeTeste.getText() )  ){
            		config.notificaUsuario("Insira um nome para a avaliação antes de cria-la.", "avisar");
            	}else{
            		if( cody.setNovoTesteByTurmaId(turmaIdSelecionado, jTextFieldNomeTeste.getText(), jFormattedTextTeste.getText()  ) ){
            			config.notificaUsuario("Avaliação inserida com sucesso!", "informar");
            			trocarDePainel("testes");
            		}else{
            			config.notificaUsuario("Não foi possível inserir a avaliação. Verifique o nome/ano e tente novamente.", "erro");
            		}
            		jTextFieldNomeTeste.setText("");
            		jFormattedTextTeste.setText("");
            	}
            }
        });
        
        jScrollPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));

        /**
         * Lista de Testes
         */
        jListTeste.setListData(new String[0]);
        jListTeste.addListSelectionListener(
				new ListSelectionListener()
				{
					@Override
					public void valueChanged( ListSelectionEvent event)
					{
						if( jListTeste.getSelectedIndex() >= 0)
						{
							escondePainelAtivo();
							testeIdSelecionado = jListTeste.getSelectedIndex();
							trocarDePainel("teste");
						}
					}
				}
				
		);
        jListTeste.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jListTeste.setFont( fonte.getFontTexto() );
        jListTeste.setDragEnabled(true);
        jScrollPane1.setViewportView(jListTeste);

        try {
            jFormattedTextTeste.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedTextTeste.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            }
        });
        
        jFormattedTextTeste.setToolTipText("Digite a data em que o teste foi realizado.");
        jTextFieldNomeTeste.setToolTipText("Digite aqui o Código/Nome do teste.");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(painelTestes);
        painelTestes.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(33, 33, 33)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabelTipoTeste, javax.swing.GroupLayout.PREFERRED_SIZE, 105, Short.MAX_VALUE)
                                    .addGap(149, 149, 149))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabelNomeTeste, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGap(116, 116, 116))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabelCriarTeste, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGap(145, 145, 145))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGap(0, 0, Short.MAX_VALUE)
                                    .addComponent(jFormattedTextTeste, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jTextFieldNomeTeste, javax.swing.GroupLayout.Alignment.TRAILING))))
                    .addGap(28, 28, 28)
                    .addComponent(jLabelIconeInfo)
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGap(23, 23, 23))
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(jLabelCriarTeste, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabelNomeTeste)
                                    .addGap(18, 18, 18)
                                    .addComponent(jTextFieldNomeTeste, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabelIconeInfo))
                            .addGap(18, 18, 18)
                            .addComponent(jFormattedTextTeste, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jButtonSalvar)
                            .addGap(17, 17, 17)
                            .addComponent(jLabelTipoTeste)
                            .addGap(18, 18, 18)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(1, 1, 1)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGap(44, 68, Short.MAX_VALUE))
            );
	}
	
	/**
	 * Cria-se o painel que exibi o teste realizado na turma como um todo
	 * 	através da grid
	 */
	public void criarTeste(){
		this.painelTeste = new JPanel();
		this.painelTeste.setVisible(false);
		
		JButton jButtonExcluir;
	    JButton jButtonSalvar;
	    JButton jButtonVoltar;
	    JLabel jLabelTeste;
	    JScrollPane jScrollPane1;
	    JScrollPane jScrollPane2;
	    JScrollPane jScrollPane3;
	    JTabbedPane jTabbedTabela;
	    
		
	    jLabelTeste = new javax.swing.JLabel();
        jButtonVoltar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jTabbedTabela = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableMedidaDeCrescimento = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableAptidaoEsportiva = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableAptidaoSaude = new javax.swing.JTable();

        painelTeste.setBackground(new java.awt.Color(255, 255, 255));

        jLabelTeste.setText("Testes da Avaliação");
        jLabelTeste.setFont( fonte.getFontTitulo() );

        jButtonVoltar.setText("Voltar");
        jButtonVoltar.setFont( fonte.getFontTexto() );
        jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	trocarDePainel("testes");
            }
        });

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.setFont( fonte.getFontTexto() );
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            
             
            	boolean retorno1 = cody.setTestesAptidaoEsportiva(turmaIdSelecionado, testeIdSelecionado, (JTableModeloTeste)jTableAptidaoEsportiva.getModel());
            	boolean retorno2 = cody.setTestesAptidaoSaude(turmaIdSelecionado, testeIdSelecionado, (JTableModeloTeste)jTableAptidaoSaude.getModel());
            	boolean retorno3 = cody.setTestesMedidaCrescimento(turmaIdSelecionado, testeIdSelecionado, (JTableModeloTeste)jTableMedidaDeCrescimento.getModel());
            	if(retorno1 && retorno2 && retorno3){
            		config.notificaUsuario("Mudanças salvas com sucesso.", "informar");
            		trocarDePainel("testes");
            	}else{
            		config.notificaUsuario("Não foi possível salvar.Tente novamente.", "erro");
            	}
            }
        });
        
        jButtonExcluir.setText("Excluir");
        jButtonExcluir.setFont( fonte.getFontTexto() );
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	int retorno;
            	retorno = config.notificarUsuarioComRetorno("Deseja remover esse teste?");
   				if(retorno == 0){
		       		 	if( cody.setExcluirTesteTurma(turmaIdSelecionado, testeIdSelecionado) ){
		       		 		config.notificaUsuario("Removido com sucesso!", "informar");
		       		 		trocarDePainel("testes");
		       		 	}else{
		       		 		config.notificaUsuario("Não foi possível remover o teste. Tente novamente.", "erro");
		       		 	}
   				}
            }
        });

        jTableMedidaDeCrescimento.setModel(new javax.swing.table.DefaultTableModel());
        jTableMedidaDeCrescimento.setFont( fonte.getFontTexto() );
        jScrollPane1.setViewportView(jTableMedidaDeCrescimento);

        jTabbedTabela.addTab("Medida de Crescimento", jScrollPane1);

        jTableAptidaoEsportiva.setModel(new javax.swing.table.DefaultTableModel());
        jTableAptidaoEsportiva.setFont( fonte.getFontTexto() );
        jScrollPane2.setViewportView(jTableAptidaoEsportiva);

        jTabbedTabela.addTab("Aptidão Esportiva", jScrollPane2);

        jTableAptidaoSaude.setModel(new javax.swing.table.DefaultTableModel());
        jTableAptidaoSaude.setFont( fonte.getFontTexto() );
        jScrollPane3.setViewportView(jTableAptidaoSaude);

        jTabbedTabela.addTab("Aptidão de Saúde", jScrollPane3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(painelTeste);
        painelTeste.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(5, 5, 5)
                            .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(12, 12, 12)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTabbedTabela, javax.swing.GroupLayout.DEFAULT_SIZE, 757, Short.MAX_VALUE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(21, 21, 21)
                                    .addComponent(jLabelTeste, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addContainerGap())
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(jLabelTeste, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(jTabbedTabela, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(64, 64, 64))
            );
	}
	
	/**
	 * Cria-se o painel que exibi o exportador, ou seja, a janela pela
	 * 	qual o usuário salva a informação no formato XML
	 */
	public void criarExportar(){
		this.painelExportar = new JPanel();
		this.painelExportar.setVisible(false);
		
		final JFileChooser jFileChooser;
	    
        jFileChooser = new javax.swing.JFileChooser();

        painelExportar.setBackground(new java.awt.Color(255, 255, 255));

        jLabelExportar.setText("Salvar como...");
        jLabelExportar.setFont( fonte.getFontTitulo() );

        jFileChooser.setDialogType(javax.swing.JFileChooser.SAVE_DIALOG);
        jFileChooser.setBackground(new java.awt.Color(254, 254, 254));
        
        ExtensionFileFilter codyFilter = new ExtensionFileFilter(new String("cody"), "Cody Files");
        jFileChooser.setAcceptAllFileFilterUsed(false);
        jFileChooser.addChoosableFileFilter( codyFilter);
        
        jFileChooser.setFont( fonte.getFontTexto() );
        jFileChooser.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	String botaoPressionado = evt.getActionCommand();
        		int ret;
            	File f = jFileChooser.getSelectedFile();
            	File fTest;
            	String fPath;			// * = Agora o arquivo salva como .cody.
		       	if(f != null && botaoPressionado.equals("ApproveSelection")){
		       		 /**
		       		  * Exportar o arquivo
		       		  */
		       		fPath = f.getAbsolutePath();							// *
	       			if(!fPath.substring(fPath.length()-5).equals(".cody")){	// *
	       				fPath = fPath + ".cody";							// *
	       			}
	       			
	       			fTest = new File(fPath);								// #
	       			if(fTest.exists()){										// #
	       				ret = config.notificarUsuarioComRetorno("Já existe um arquivo com este nome. Deseja salvar sobre o que está escrito?");
	       				if(ret == 0){
	   		       		 	if(cody.exportarArquivo(fPath)){
	   		       		 		config.notificaUsuario("Arquivo salvo com sucesso!", "informar");
	   		       		 		trocarDePainel("home");
	   		       		 	}else{
	   		       		 		config.notificaUsuario("Não foi possível salvar o arquivo. Tente novamente.", "erro");
	   		       		 	}
	       				}
	       				else{
	       					if(ret == 2){
	       						trocarDePainel("home");
	       					}
	       				}
	       			}else{
	       				/**
	       				 * Cria o arquivo do zero
	       				 */
	       				if(cody.exportarArquivo(fPath)){
   		       		 		config.notificaUsuario("Arquivo criado com sucesso.", "informar");
   		       		 		trocarDePainel("home");	       					
	       				}else{
   		       		 		config.notificaUsuario("Não foi possível criar o arquivo. Tente novamente.", "erro");
   		       		 	}
	       			}
		       	 }else{
		       		 trocarDePainel("home");
		       	 }
            }
        });
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(painelExportar);
        painelExportar.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelExportar, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jFileChooser, javax.swing.GroupLayout.DEFAULT_SIZE, this.tamTela.width, Short.MAX_VALUE)
                        .addGap(21, 21, 21))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabelExportar, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jFileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, this.tamTela.height - 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
        );
	}
	
	/**
	 * Cria-se o painel de ajuda, contendo um help simples
	 * 	para o usuário
	 */
	private void criarAjuda(){
		this.painelAjuda = new JPanel();
		this.painelAjuda.setVisible(false);
		
		JButton jButtonVoltar;
	    JLabel jLabel1;
	    JLabel jLabel10;
	    JLabel jLabel11;
	    JLabel jLabel12;
	    JLabel jLabel13;
	    JLabel jLabel14;
	    JLabel jLabel15;
	    JLabel jLabel16;
	    JLabel jLabel17;
	    JLabel jLabel18;
	    JLabel jLabel4;
	    JLabel jLabel5;
	    JLabel jLabel6;
	    JLabel jLabel7;
	    JLabel jLabel8;
	    JLabel jLabel9;
	    JLabel jLabelOqueProdown;
	    JLabel jLabelProdownComoAjudar;
	    JLabel jLabelTeste;
	    JPanel jPanel1;
	    JPanel jPanel2;
	    JPanel jPanel3;
	    JPanel jPanel4;
	    JPanel jPanel5;
	    JScrollPane jScrollPane10;
	    JScrollPane jScrollPane11;
	    JScrollPane jScrollPane12;
	    JScrollPane jScrollPane13;
	    JScrollPane jScrollPane14;
	    JScrollPane jScrollPane15;
	    JScrollPane jScrollPane16;
	    JScrollPane jScrollPane17;
	    JScrollPane jScrollPane18;
	    JScrollPane jScrollPane2;
	    JScrollPane jScrollPane3;
	    JScrollPane jScrollPane4;
	    JScrollPane jScrollPane5;
	    JScrollPane jScrollPane6;
	    JScrollPane jScrollPane7;
	    JScrollPane jScrollPane8;
	    JScrollPane jScrollPane9;
	    JScrollPane jScrollPaneGeral;
	    JTabbedPane jTabbedPane1;
	    JTextArea jTextAreaDescricao;
	    JTextArea jTextAreaDescricao1;
	    JTextArea jTextAreaDescricao10;
	    JTextArea jTextAreaDescricao11;
	    JTextArea jTextAreaDescricao12;
	    JTextArea jTextAreaDescricao13;
	    JTextArea jTextAreaDescricao14;
	    JTextArea jTextAreaDescricao15;
	    JTextArea jTextAreaDescricao16;
	    JTextArea jTextAreaDescricao2;
	    JTextArea jTextAreaDescricao3;
	    JTextArea jTextAreaDescricao4;
	    JTextArea jTextAreaDescricao5;
	    JTextArea jTextAreaDescricao6;
	    JTextArea jTextAreaDescricao7;
	    JTextArea jTextAreaDescricao8;
	    JTextArea jTextAreaDescricao9;
	    
	    jLabelTeste = new javax.swing.JLabel();
        jButtonVoltar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPaneGeral = new javax.swing.JScrollPane();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabelProdownComoAjudar = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaDescricao = new javax.swing.JTextArea();
        jLabelOqueProdown = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextAreaDescricao1 = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextAreaDescricao2 = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextAreaDescricao3 = new javax.swing.JTextArea();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTextAreaDescricao4 = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTextAreaDescricao5 = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTextAreaDescricao6 = new javax.swing.JTextArea();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTextAreaDescricao7 = new javax.swing.JTextArea();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        jTextAreaDescricao8 = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        jTextAreaDescricao9 = new javax.swing.JTextArea();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane12 = new javax.swing.JScrollPane();
        jTextAreaDescricao10 = new javax.swing.JTextArea();
        jLabel13 = new javax.swing.JLabel();
        jScrollPane13 = new javax.swing.JScrollPane();
        jTextAreaDescricao11 = new javax.swing.JTextArea();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane14 = new javax.swing.JScrollPane();
        jTextAreaDescricao12 = new javax.swing.JTextArea();
        jPanel5 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jScrollPane15 = new javax.swing.JScrollPane();
        jTextAreaDescricao13 = new javax.swing.JTextArea();
        jLabel16 = new javax.swing.JLabel();
        jScrollPane16 = new javax.swing.JScrollPane();
        jTextAreaDescricao14 = new javax.swing.JTextArea();
        jScrollPane17 = new javax.swing.JScrollPane();
        jTextAreaDescricao15 = new javax.swing.JTextArea();
        jScrollPane18 = new javax.swing.JScrollPane();
        jTextAreaDescricao16 = new javax.swing.JTextArea();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();

        painelAjuda.setBackground(new java.awt.Color(255, 255, 255));

        jLabelTeste.setText("Ajuda");

        jButtonVoltar.setText("Voltar");
        jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	trocarDePainel("home");
            }
        });

        jLabel1.setText("Se você está precisando de ajuda esse é o local certo. Aqui há uma série de informações básicas para auxiliá-lo. Selecione uma aba de acordo com suas dúvida.");

        jScrollPaneGeral.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTabbedPane1.setBorder(null);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(null);

        jLabelProdownComoAjudar.setText("Como posso entrar em contato?");

        jScrollPane2.setBorder(null);
        jScrollPane2.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        jTextAreaDescricao.setEditable(false);
        jTextAreaDescricao.setColumns(20);
        jTextAreaDescricao.setLineWrap(true);
        jTextAreaDescricao.setRows(5);
        jTextAreaDescricao.setText("ProDown é uma associação sem fins lucrativos para atendimentos especializados de crianças com necessidades especiais, tendo um Centro de Equoterapia e profissionais da área de Psicologia, Fisioterapia, Educação Especial e Pedagogia.");
        jTextAreaDescricao.setToolTipText("");
        //jTextAreaDescricao.set
        jTextAreaDescricao.setBorder(null);
        jScrollPane2.setViewportView(jTextAreaDescricao);

        jLabelOqueProdown.setText("O que é o Projeto Prodown?");

        jScrollPane3.setBorder(null);
        jScrollPane3.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao1.setEditable(false);
        jTextAreaDescricao1.setColumns(20);
        jTextAreaDescricao1.setLineWrap(true);
        jTextAreaDescricao1.setRows(5);
        jTextAreaDescricao1.setText("Não encontrou informações para solucionar seu problema? Estamos disponíveis na Central de Ajuda do site http://www.prodown.com.br/ajuda ou do email contato@prodown.com.br para responder suas dúvidas ou curiosidades sobre o projeto.");
        jTextAreaDescricao1.setToolTipText("");
        jTextAreaDescricao1.setWrapStyleWord(true);
        jTextAreaDescricao1.setBorder(null);
        jScrollPane3.setViewportView(jTextAreaDescricao1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelOqueProdown)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelProdownComoAjudar)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jScrollPane3)))
                .addGap(323, 323, 323))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelOqueProdown)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelProdownComoAjudar)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(286, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("ProDown", jPanel1);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel4.setText("O que é uma \"Instituição\"?");

        jScrollPane4.setBorder(null);
        jScrollPane4.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao2.setEditable(false);
        jTextAreaDescricao2.setColumns(20);
        jTextAreaDescricao2.setLineWrap(true);
        jTextAreaDescricao2.setRows(5);
        jTextAreaDescricao2.setText("Uma Instituição é toda e qualquer escola de ensino regular ou especial que você deseja cadastrar turmas, alunos e seus avaliações.");
        jTextAreaDescricao2.setToolTipText("");
        jTextAreaDescricao2.setWrapStyleWord(true);
        jTextAreaDescricao2.setBorder(null);
        jScrollPane4.setViewportView(jTextAreaDescricao2);

        jLabel5.setText("Porque preciso de CNPJ para minha Instituição?");

        jScrollPane5.setBorder(null);
        jScrollPane5.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane5.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao3.setEditable(false);
        jTextAreaDescricao3.setColumns(20);
        jTextAreaDescricao3.setLineWrap(true);
        jTextAreaDescricao3.setRows(5);
        jTextAreaDescricao3.setText("Toda instituição de ensino possui um CNPJ para a sua identifição. Nos usamos o CNPJ para identificar a sua instituição no sistema online. Observe que essa informação não é repassado a terceiros.");
        jTextAreaDescricao3.setToolTipText("");
        jTextAreaDescricao3.setWrapStyleWord(true);
        jTextAreaDescricao3.setBorder(null);
        jScrollPane5.setViewportView(jTextAreaDescricao3);

        jLabel6.setText("Como obter o CNPJ da instituição?");

        jScrollPane6.setBorder(null);
        jScrollPane6.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane6.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao4.setEditable(false);
        jTextAreaDescricao4.setColumns(20);
        jTextAreaDescricao4.setLineWrap(true);
        jTextAreaDescricao4.setRows(5);
        jTextAreaDescricao4.setText("Entre em contato com a direção da sua instituição de ensino para obter o CNPJ.");
        jTextAreaDescricao4.setToolTipText("");
        jTextAreaDescricao4.setWrapStyleWord(true);
        jTextAreaDescricao4.setBorder(null);
        jScrollPane6.setViewportView(jTextAreaDescricao4);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                            .addGap(26, 26, 26)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)
                                .addComponent(jScrollPane4)))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                            .addGap(26, 26, 26)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel4)
                                .addComponent(jLabel6))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                    .addComponent(jLabel5))
                .addGap(312, 312, 312))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(152, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Instituição", jPanel2);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel7.setText("O que é uma \"Turma\"?");

        jScrollPane7.setBorder(null);
        jScrollPane7.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane7.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao5.setEditable(false);
        jTextAreaDescricao5.setColumns(20);
        jTextAreaDescricao5.setLineWrap(true);
        jTextAreaDescricao5.setRows(5);
        jTextAreaDescricao5.setText("Uma turma, no nosso sistema, é o conjunto de todos os alunos com sindrome de Down, com idade entre 10 e 20 anos e que esteja matriculado em sua instituição. E que vão ou já realizam as avaliações de aptidões.");
        jTextAreaDescricao5.setToolTipText("");
        jTextAreaDescricao5.setWrapStyleWord(true);
        jTextAreaDescricao5.setBorder(null);
        jScrollPane7.setViewportView(jTextAreaDescricao5);

        jLabel8.setText("Como criar mais de uma turma?");

        jScrollPane8.setBorder(null);
        jScrollPane8.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane8.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao6.setEditable(false);
        jTextAreaDescricao6.setColumns(20);
        jTextAreaDescricao6.setLineWrap(true);
        jTextAreaDescricao6.setRows(5);
        jTextAreaDescricao6.setText("Você pode criar quantas turmas forem de seu interesse, respeitando a regra de haver apenas uma turma com o mesmo nome por ano, mas lembre-se que essas turmas estão vinculados a esta instituição. Caso a turma que você precise cadastrar é de outra instituição, adicione uma nova instituição através de outro arquivo.");
        jTextAreaDescricao6.setToolTipText("");
        jTextAreaDescricao6.setWrapStyleWord(true);
        jTextAreaDescricao6.setBorder(null);
        jScrollPane8.setViewportView(jTextAreaDescricao6);

        jLabel9.setText("O que fazer com alunos que sairam ou mudaram de turma?");

        jScrollPane9.setBorder(null);
        jScrollPane9.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane9.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao7.setEditable(false);
        jTextAreaDescricao7.setColumns(20);
        jTextAreaDescricao7.setLineWrap(true);
        jTextAreaDescricao7.setRows(5);
        jTextAreaDescricao7.setText("Remova o aluno de uma turma e ensira-o em uma outra, porém lembre-se que ao remover o aluno os testes serão removidos do sistema. Se o arquivo já foi alguma vez carregado no sistema online as informações anteriores serão preservadas no sistema online e não do desktop.");
        jTextAreaDescricao7.setToolTipText("");
        jTextAreaDescricao7.setWrapStyleWord(true);
        jTextAreaDescricao7.setBorder(null);
        jScrollPane9.setViewportView(jTextAreaDescricao7);

        jLabel10.setText("Posso remover a turma sem remover os alunos?");

        jScrollPane10.setBorder(null);
        jScrollPane10.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane10.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao8.setEditable(false);
        jTextAreaDescricao8.setColumns(20);
        jTextAreaDescricao8.setLineWrap(true);
        jTextAreaDescricao8.setRows(5);
        jTextAreaDescricao8.setText("Não, pois você precisa ter os alunos vinculados a uma turma. Para remover a turma e não perder as informações, aconselhamos que você salve as informações do aluno que deseja ou renomeia a turma e remova os demais alunos.");
        jTextAreaDescricao8.setToolTipText("");
        jTextAreaDescricao8.setWrapStyleWord(true);
        jTextAreaDescricao8.setBorder(null);
        jScrollPane10.setViewportView(jTextAreaDescricao8);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)
                            .addComponent(jScrollPane9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)
                            .addComponent(jScrollPane10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE))))
                .addGap(312, 312, 312))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel10)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Turma", jPanel3);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jLabel11.setText("O que é um \"Aluno\"?");

        jScrollPane11.setBorder(null);
        jScrollPane11.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane11.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao9.setEditable(false);
        jTextAreaDescricao9.setColumns(20);
        jTextAreaDescricao9.setLineWrap(true);
        jTextAreaDescricao9.setRows(5);
        jTextAreaDescricao9.setText("Um Aluno é uma criança ou adolecente com sindrome de Down com idade entre 10 e 20 anos e que está matriculado em uma turma da sua instiuição de ensino. Este aluno fez ou fará os testes e os resultados serão inseridos aqui.");
        jTextAreaDescricao9.setToolTipText("");
        jTextAreaDescricao9.setWrapStyleWord(true);
        jTextAreaDescricao9.setBorder(null);
        jScrollPane11.setViewportView(jTextAreaDescricao9);

        jLabel12.setText("Como inserir vários alunos?");

        jScrollPane12.setBorder(null);
        jScrollPane12.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane12.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao10.setEditable(false);
        jTextAreaDescricao10.setColumns(20);
        jTextAreaDescricao10.setLineWrap(true);
        jTextAreaDescricao10.setRows(5);
        jTextAreaDescricao10.setText("Você pode inserir vários alunos para cada uma das turmas, mas lembre-se que todo aluno deve pertencer a exclusivamente uma turma dentro do ano vigente.");
        jTextAreaDescricao10.setToolTipText("");
        jTextAreaDescricao10.setWrapStyleWord(true);
        jTextAreaDescricao10.setBorder(null);
        jScrollPane12.setViewportView(jTextAreaDescricao10);

        jLabel13.setText("O que fazer com alunos que sairam ou mudaram de turma?");

        jScrollPane13.setBorder(null);
        jScrollPane13.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane13.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao11.setEditable(false);
        jTextAreaDescricao11.setColumns(20);
        jTextAreaDescricao11.setLineWrap(true);
        jTextAreaDescricao11.setRows(5);
        jTextAreaDescricao11.setText("Simplesmente remova o aluno de uma turma e o ensira em uma outra, porém lembre-se que ao remover os alunos serão removidos os testes do aluno! Caso os dados forem carregado para o sistema online você poderá resgata-los.");
        jTextAreaDescricao11.setToolTipText("");
        jTextAreaDescricao11.setWrapStyleWord(true);
        jTextAreaDescricao11.setBorder(null);
        jScrollPane13.setViewportView(jTextAreaDescricao11);

        jLabel14.setText("Posso remover um aluno sem remover seus testes?");

        jScrollPane14.setBorder(null);
        jScrollPane14.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane14.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao12.setEditable(false);
        jTextAreaDescricao12.setColumns(20);
        jTextAreaDescricao12.setLineWrap(true);
        jTextAreaDescricao12.setRows(5);
        jTextAreaDescricao12.setText("Não, pois você precisa ter os testes vinculados a um aluno e caso remova o aluno seus testes serão permanetemente perdidos. Caso os dados forem carregados no sistema Web, os dados não serão removidos e você poderá resgata-los");
        jTextAreaDescricao12.setToolTipText("");
        jTextAreaDescricao12.setWrapStyleWord(true);
        jTextAreaDescricao12.setBorder(null);
        jScrollPane14.setViewportView(jTextAreaDescricao12);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)
                            .addComponent(jScrollPane12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)
                            .addComponent(jScrollPane13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)
                            .addComponent(jScrollPane14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE))))
                .addGap(312, 312, 312))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel12)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel13)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel14)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Aluno", jPanel4);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        jLabel15.setText("O que é uma \"Avaliação\"?");

        jScrollPane15.setBorder(null);
        jScrollPane15.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane15.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao13.setEditable(false);
        jTextAreaDescricao13.setColumns(20);
        jTextAreaDescricao13.setLineWrap(true);
        jTextAreaDescricao13.setRows(5);
        jTextAreaDescricao13.setText("Uma Avaliação é o conjunto de três testes de aptidões diferentes: \"Teste de Aptidão Física\", \"Teste de Aptidão de Saúde\" e \"Teste de Crescimento\". Sendo cada um deles um conjunto de informações sobre a ação do aluno");
        jTextAreaDescricao13.setToolTipText("");
        jTextAreaDescricao13.setWrapStyleWord(true);
        jTextAreaDescricao13.setBorder(null);
        jScrollPane15.setViewportView(jTextAreaDescricao13);

        jLabel16.setText("Como devo realizar/Aplicar os testes da Avaliação?");

        jScrollPane16.setBorder(null);
        jScrollPane16.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane16.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao14.setEditable(false);
        jTextAreaDescricao14.setColumns(20);
        jTextAreaDescricao14.setLineWrap(true);
        jTextAreaDescricao14.setRows(5);
        jTextAreaDescricao14.setText("Você deve acessar o portal do Prodown http://www.prodown.com.br para ter acesso as vídeos e documentação que mostra como realizar corretamente o levantamento das informações");
        jTextAreaDescricao14.setToolTipText("");
        jTextAreaDescricao14.setWrapStyleWord(true);
        jTextAreaDescricao14.setBorder(null);
        jScrollPane16.setViewportView(jTextAreaDescricao14);

        jScrollPane17.setBorder(null);
        jScrollPane17.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane17.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao15.setEditable(false);
        jTextAreaDescricao15.setColumns(20);
        jTextAreaDescricao15.setLineWrap(true);
        jTextAreaDescricao15.setRows(5);
        jTextAreaDescricao15.setText("Entre em \"Avaliações da Turma\" e insira um nome do Avaliação e a data de ocorrência e clique em \"criar\". Pronto, sua turma terá uma avaliação e basta que você digite, acessando os testes, os valores obtidos anteriormente ou que estão na sua planilha.");
        jTextAreaDescricao15.setToolTipText("");
        jTextAreaDescricao15.setWrapStyleWord(true);
        jTextAreaDescricao15.setBorder(null);
        jScrollPane17.setViewportView(jTextAreaDescricao15);

        jScrollPane18.setBorder(null);
        jScrollPane18.setForeground(new java.awt.Color(254, 254, 254));
        jScrollPane18.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaDescricao16.setEditable(false);
        jTextAreaDescricao16.setColumns(20);
        jTextAreaDescricao16.setLineWrap(true);
        jTextAreaDescricao16.setRows(5);
        jTextAreaDescricao16.setText("Infelizmente não, você quando obta por remover a avaliação, o sistema remove todos os seus três testes de aptidão e não é possível reverter a remoção depois de salvo no arquivo.");
        jTextAreaDescricao16.setToolTipText("");
        jTextAreaDescricao16.setWrapStyleWord(true);
        jTextAreaDescricao16.setBorder(null);
        jScrollPane18.setViewportView(jTextAreaDescricao16);

        jLabel17.setText("Posso remover uma Avaliação sem remover os valores digitados?");

        jLabel18.setText("Como criar vários Testes?");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane18, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)
                            .addComponent(jScrollPane15, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane16, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane17))))
                .addGap(312, 312, 312))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel16)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel18)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel17)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Teste", jPanel5);

        jScrollPaneGeral.setViewportView(jTabbedPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(painelAjuda);
        painelAjuda.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 731, Short.MAX_VALUE)
                    .addComponent(jScrollPaneGeral, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jLabelTeste, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabelTeste, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPaneGeral, javax.swing.GroupLayout.DEFAULT_SIZE, 384, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
	}
	
	/**
	 * Esconde todos os paineis exceto o painel cabelhaço
	 */
    private void escondePainelAtivo(){
    	this.painelHome.setVisible(false);
		this.painelAluno.setVisible(false);
		this.painelAlunos.setVisible(false);
		this.painelCriarInstituicao.setVisible(false);
		this.painelImportar.setVisible(false);
		this.painelTeste.setVisible(false);
		this.painelTestes.setVisible(false);
		this.painelTurma.setVisible(false);
		this.painelTurmas.setVisible(false);
		this.painelExportar.setVisible(false);
		this.painelAjuda.setVisible(false);
    }
	
    /**
	 * Método que é responsável pela troca entre os paineis
	 * 	e é claro uma série de troca entre regras de negócio do sistema
	 * @param painel		O painel a ser trocado.
	 */
	public void trocarDePainel(String painel){
		escondePainelAtivo();
		
		if("importar".equals(painel)){
			escolheAbrirArquivo();
		}else{
			if("home".equals(painel)){
				escolheHome();
			}else{
				if("aluno".equals(painel)){
					escolheAluno();
				}else{
					if("alunos".equals(painel)){
						escolheAlunos();
					}else{
						if("turmas".equals(painel)){
							escolheTurmas();
						}else{
							if("turma".equals(painel)){
								escolheTurma();
							}else{
								if("teste".equals(painel)){
									escolheTeste();
								}else{
									if("testes".equals(painel)){
										escolheTestes();
									}else{
										if("instituicao".equals(painel)){
											escolheInstituicao();
										}else{
											if("exportar".equals(painel)){
												escolheExportar();
											}else{
												if("ajuda".equals(painel)){
													escolheAjuda();
												}else{
													if("criarInstituicao".equals(painel)){
														escolheCriarInstituicao();
													}else{
														this.painelHome.setVisible(true);
														painelAtivo = "home";
														config.notificaUsuario("Oops! "+painel+" nao existe", null);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	
	public void escolheHome(){
		if( cody.temArquivoAberto() || cody.temDadosParaSalvar() ){
			this.jListTurmas.setListData( cody.getListaTurmas() );
			this.painelTurmas.setVisible(true);
			this.cabecaLabelTexto.setText("Turmas");
			this.cabecaLabelImagem.setIcon(icones.getIcone("turmas"));
			this.painelAtivo = "turmas";
		}else{
			this.painelHome.setVisible(true);
			this.cabecaLabelTexto.setText("Bem-Vindo ao Projeto ProDown");
			this.cabecaLabelImagem.setIcon(icones.getIcone("home"));
			this.painelAtivo = "home";
		}
	}
	
	
	public void escolheTurma(){
		if( turmaIdSelecionado >= 0 && ( this.painelAtivo == "exportar" || this.painelAtivo == "turma" || this.painelAtivo == "turmas" || this.painelAtivo == "testes" || this.painelAtivo == "teste" || this.painelAtivo == "aluno" || this.painelAtivo == "alunos" ) ){
			this.jListAlunos.setListData( cody.getListaAlunosByTurmaId(turmaIdSelecionado) );
			this.painelTurma.setVisible(true);
			this.cabecaLabelTexto.setText("Turma "+cody.getNomeTurmaById( turmaIdSelecionado ) );
			this.jTextFieldNomeTurma.setText( cody.getNomeTurmaById( turmaIdSelecionado ) );
			this.jTurmaAno.setText( cody.getTurmaAnoById(turmaIdSelecionado) );
			this.cabecaLabelImagem.setIcon(icones.getIcone("turma"));
			this.painelAtivo = "turma";
			
		}else{
			this.escolheHome();
			config.notificaUsuario("Para se ver uma turma é necessário primeiro criar uma turma em Turmas", "avisar");
		}
	}
	
	private void escolheTestes(){
		if( turmaIdSelecionado >= 0 && (this.painelAtivo == "turma" || this.painelAtivo == "alunos" || this.painelAtivo == "aluno" || this.painelAtivo == "testes" || this.painelAtivo == "teste") ){
			jListTeste.setListData(cody.getListaTestesByTurma(turmaIdSelecionado));
			this.painelTestes.setVisible(true);
			this.cabecaLabelTexto.setText("Avaliações da turma "+cody.getNomeTurmaById( turmaIdSelecionado ) );
			this.jTextFieldNomeTurma.setText( cody.getNomeTurmaById( turmaIdSelecionado ) );
			this.cabecaLabelImagem.setIcon(icones.getIcone("testes"));
			this.jListTeste.setListData(cody.getListaTestesByTurma(turmaIdSelecionado));
			this.painelAtivo = "testes";
		}else{
			this.escolheHome();
			config.notificaUsuario("Para ver os Testes é necessário estar em uma Turma.", "avisar");
		}
	}
	
	private void escolheTeste(){
		if(testeIdSelecionado >= 0 && (this.painelAtivo == "turma" || this.painelAtivo == "alunos" || this.painelAtivo == "aluno" || this.painelAtivo == "testes" || this.painelAtivo == "teste")){
			this.painelTeste.setVisible(true);
			this.cabecaLabelTexto.setText("Avaliação da turma "+cody.getNomeTurmaById(turmaIdSelecionado));
			this.cabecaLabelImagem.setIcon(icones.getIcone("teste"));
			this.painelAtivo = "teste";
			this.jTableAptidaoEsportiva.setModel( cody.getTestesAptidaoEsportiva(this.turmaIdSelecionado,this.testeIdSelecionado) );
			this.jTableAptidaoSaude.setModel( cody.getTestesAptidaoSaude(turmaIdSelecionado, testeIdSelecionado) );
			this.jTableMedidaDeCrescimento.setModel( cody.getTestesMedidaCrescimento(turmaIdSelecionado, testeIdSelecionado) );
		}else{
			this.escolheHome();
			config.notificaUsuario("Para ver o Teste é necessário escolher uma turma.", "avisar");
		}
	}
	
	private void escolheCriarInstituicao(){
		if(cody.temDadosParaSalvar()){
			painelAtivo = "turmas";
			this.painelTurmas.setVisible(true);
			int retorno  = config.notificarUsuarioComRetorno("Há dados para salvar, deseja salvar estes dados antes de criar uma nova instituição?") ;
			switch(retorno){
				case 0:
					this.painelTurmas.setVisible(true);
					if(null == cody.getArquivoAberto()){
						this.painelTurmas.setVisible(false);
		        		this.trocarDePainel("exportar");
		        	}else{
		        		boolean gravouComSucesso;
		        		gravouComSucesso = cody.exportarArquivo(cody.getArquivoAberto());
	    				if( gravouComSucesso == false)
	    				{
	    					JOptionPane.showMessageDialog(null, "Atenção não foi possível salvar as modificações. Tente novamente.");
	    				}else{
	    					cody.resetAll();
	    					this.cabecaLabelTexto.setText("Criar Instituição");
	    					this.cabecaLabelImagem.setIcon(icones.getIcone("instituicao"));
	    					painelAtivo = "instituicao";
	    					this.painelTurmas.setVisible(false);
	    					this.painelCriarInstituicao.setVisible(true);
	    				}
		        	}
					break;
				case 1:
					cody.resetAll();
					this.cabecaLabelTexto.setText("Criar Instituição");
					this.cabecaLabelImagem.setIcon(icones.getIcone("instituicao"));
					painelAtivo = "instituicao";
					this.painelTurmas.setVisible(false);
					this.painelCriarInstituicao.setVisible(true);
					break;
			}
		}else{
			this.painelCriarInstituicao.setVisible(true);
			if(cody.temArquivoAberto() || cody.temDadosParaSalvar() ){
				this.cabecaLabelTexto.setText("Editar Instituição");
				this.cabecaLabelImagem.setIcon(icones.getIcone("instituicao"));
			}else{
				this.cabecaLabelTexto.setText("Criar Instituição");
				this.cabecaLabelImagem.setIcon(icones.getIcone("instituicao"));
			}
			painelAtivo = "instituicao";
		}
	}
	
	private void escolheInstituicao(){
		if(cody.temArquivoAberto()){
			this.painelTurmas.setVisible(true);
			painelAtivo = "turmas";
			instituicao.tornarVisivel(true);
			
		}else{
			this.painelCriarInstituicao.setVisible(true);
			if(cody.temArquivoAberto() || cody.temDadosParaSalvar() ){
				this.cabecaLabelTexto.setText("Editar Instituição");
				this.cabecaLabelImagem.setIcon(icones.getIcone("instituicao"));
			}else{
				this.cabecaLabelTexto.setText("Criar Instituição");
				this.cabecaLabelImagem.setIcon(icones.getIcone("instituicao"));
			}
			painelAtivo = "instituicao";
		}
	}
	
	private void escolheExportar(){
		if( !cody.temDadosParaSalvar() || cody.getArquivoAberto() == null){
			this.painelExportar.setVisible(true);
			this.cabecaLabelTexto.setText("Salvar");
			this.jLabelExportar.setText("Salvar arquivo");
			this.cabecaLabelImagem.setIcon(icones.getIcone("salvar"));
		}else{
			this.painelExportar.setVisible(true);
			if(cody.getArquivoAberto() == null){
				this.cabecaLabelTexto.setText("Salvar");
				this.jLabelExportar.setText("Salvar arquivo");
				this.cabecaLabelImagem.setIcon(icones.getIcone("salvar"));
			}else{
				this.cabecaLabelTexto.setText("Salvar Como...");
				this.jLabelExportar.setText("Salvar arquivo como");
				this.cabecaLabelImagem.setIcon(icones.getIcone("salvarComo"));
			}
			
			
			this.painelAtivo = "exportar";
		}
	}
	
	
	private void escolheTurmas(){
		if(cody.temArquivoAberto() || cody.temDadosParaSalvar()){
			this.painelTurmas.setVisible(true);
			this.cabecaLabelTexto.setText("Turmas");
			this.cabecaLabelImagem.setIcon(icones.getIcone("turmas"));
			this.jListTurmas.setListData( cody.getListaTurmas() );
			this.jTurmaNovaAno.setText("");
			this.jTextFieldNomeTurma.setText("");
			painelAtivo = "turmas";
		}else{
			this.escolheHome();
			config.notificaUsuario("É necessário ter criado uma instituição antes.", "avisar");
		}
		
	}
	
	
	private void escolheAlunos(){
		if(turmaIdSelecionado >=0 && (this.painelAtivo == "turma" || this.painelAtivo == "aluno" || this.painelAtivo == "alunos") ){
			this.painelAlunos.setVisible(true);
			this.jTextFieldNomeAlunoTemp.setText("");
			this.cabecaLabelTexto.setText("Alunos da turma "+cody.getNomeTurmaById(turmaIdSelecionado));
			this.cabecaLabelImagem.setIcon(icones.getIcone("alunos"));
			this.jListAlunos.setListData( cody.getListaAlunosByTurmaId(turmaIdSelecionado) );
		}else{
			this.escolheHome();
			this.painelAtivo = "home";
			config.notificaUsuario("Para ver Alunos é necessário estar em uma turma.", "avisar");
		}
	}
	
	private void escolheAluno(){
		if( turmaIdSelecionado >=0 && (this.painelAtivo == "turma" || this.painelAtivo == "aluno" || this.painelAtivo == "alunos") ){
			//alunoId = -1 então cria um novo
			this.painelAtivo = "aluno";
			this.painelAluno.setVisible(true);
			this.cabecaLabelImagem.setIcon(icones.getIcone("aluno"));
			if(this.alunoIdSelecionado == -1){
				this.cabecaLabelTexto.setText("Novo Aluno na turma "+cody.getNomeTurmaById(turmaIdSelecionado));
				jTextFieldNomeAluno.setText(this.nomeAlunoSelecionado);
				jFormattedTextFieldCPF.setText("");
				jFormattedTextFieldData.setText("");
				jTextFieldNomeMaeAluno.setText("");
				jTextFieldNomePaiAluno.setText("");
				jTextFieldRG.setText("");
				jTextFieldEnderecoAluno.setText("");
				jButtonExcluir.setEnabled(false);
			}else{
				Aluno alunoTemp = cody.getAlunoIdByTurmaId(this.turmaIdSelecionado, this.alunoIdSelecionado);
				this.cabecaLabelTexto.setText("Editando Aluno da turma "+cody.getNomeTurmaById(turmaIdSelecionado));
				jTextFieldNomeAluno.setText( alunoTemp.getNome() );
				jFormattedTextFieldCPF.setText( alunoTemp.getcadPessoaFisica());
				jFormattedTextFieldData.setText( alunoTemp.getDataNascimento() );
				jButtonExcluir.setEnabled(true);
				if(alunoTemp.getGenero().equals("Masculino")){
					jComboBoxTipoGenero.setSelectedIndex(0);

				}else{
					jComboBoxTipoGenero.setSelectedIndex(1);
				}
	
				jTextFieldNomeMaeAluno.setText( alunoTemp.getNomeMae() );
				jTextFieldNomePaiAluno.setText( alunoTemp.getNomePai() );
				jTextFieldRG.setText( alunoTemp.getRegistroGeral() );
				jTextFieldEnderecoAluno.setText( alunoTemp.getEndereco() );
			}
		}else{
			this.escolheHome();
			this.painelAtivo = "home";
			config.notificaUsuario("Para ver o Aluno é necessário criar ou editar um Aluno.", "avisar");
		}
		
	}
	
	private void escolheAbrirArquivo(){
		if(cody.temDadosParaSalvar()){
			int retorno;
			this.painelHome.setVisible(true);
			painelAtivo = "home";
			retorno = config.notificarUsuarioComRetorno("Deseja Salvar as informações que possúe antes?");
			if(retorno == 0){
				this.trocarDePainel("exportar");
			}else{
				if(retorno == 1){
					config.notificaUsuario("Não foram salvas as modificações", "avisar");
					this.painelHome.setVisible(false);
					this.painelImportar.setVisible(true);
					this.cabecaLabelTexto.setText("Abrir Arquivo");
					this.cabecaLabelImagem.setIcon(icones.getIcone("importar"));
					painelAtivo = "importar";
				}
			}
		}else{
			this.painelImportar.setVisible(true);
			this.cabecaLabelTexto.setText("Abrir Arquivo");
			this.cabecaLabelImagem.setIcon(icones.getIcone("importar"));
			painelAtivo = "importar";
		}
	}
	
	private void escolheAjuda(){
		this.painelAjuda.setVisible(true);
		this.cabecaLabelTexto.setText("Central de Ajuda");
		this.cabecaLabelImagem.setIcon(icones.getIcone("ajuda"));
		painelAtivo = "ajuda";
	}
}

class ExtensionFileFilter extends FileFilter {  
    private static String TYPE_UNKNOWN = "Type Unknown";  
    private static String HIDDEN_FILE = "Hidden File";  
    private Hashtable filters = null;  
    private String description = null;  
    private String fullDescription = null;  
    private boolean useExtensionsInDescription = true;  
    /** 
     * Creates a file filter. If no filters are added, then all 
     * files are accepted. 
     * 
     * @see #addExtension 
     */  
    public ExtensionFileFilter() {  
    	this.filters = new Hashtable();  
    }  
    /** 
     * Creates a file filter that accepts files with the given extension. 
     * Example: new ExampleFileFilter("jpg"); 
     * 
     * @see #addExtension 
     */  
    public ExtensionFileFilter(String extension) {  
    	this(extension,null);  
    }  
    /** 
     * Creates a file filter that accepts the given file type. 
     * Example: new ExampleFileFilter("jpg", "JPEG Image Images"); 
     * 
     * Note that the "." before the extension is not needed. If 
     * provided, it will be ignored. 
     * 
     * @see #addExtension 
     */  
    public ExtensionFileFilter(String extension, String description) {  
    	this();  
    	if(extension!=null) addExtension(extension);  
    		if(description!=null) setDescription(description);  
    }  
    /** 
     * Creates a file filter from the given string array. 
     * Example: new ExampleFileFilter(String {"gif", "jpg"}); 
     * 
     * Note that the "." before the extension is not needed adn 
     * will be ignored. 
     * 
     * @see #addExtension 
     */  
    public ExtensionFileFilter(String[] filters) {  
    	this(filters, null);  
    }  
    /** 
     * Creates a file filter from the given string array and description. 
     * Example: new ExampleFileFilter(String {"gif", "jpg"}, "Gif and JPG Images"); 
     * 
     * Note that the "." before the extension is not needed and will be ignored. 
     * 
     * @see #addExtension 
     */  
    public ExtensionFileFilter(String[] filters, String description) {  
    	this();  
    	for (int i = 0; i < filters.length; i++) {  
    		// add filters one by one  
    		addExtension(filters[i]);  
    	}  
    	if(description!=null) setDescription(description);  
    }
    
    /** 
     * Return true if this file should be shown in the directory pane, 
     * false if it shouldn't. 
     * 
     * Files that begin with "." are ignored. 
     * 
     */  
    public boolean accept(File f) {  
		if(f != null) {  
		    if(f.isDirectory()) {  
		return true;  
		    }  
		    String extension = getExtension(f);  
		    if(extension != null && filters.get(getExtension(f)) != null) {  
		return true;  
		    };  
		}  
		return false;  
    }  
    /** 
     * Return the extension portion of the file's name . 
     * 
     */  
     public String getExtension(File f) {  
		if(f != null) {  
		    String filename = f.getName();  
		    int i = filename.lastIndexOf('.');  
		    if(i>0 && i<filename.length()-1) {  
		return filename.substring(i+1).toLowerCase();  
		    };  
		}  
		return null;  
    }  
    /** 
     * Adds a filetype "dot" extension to filter against. 
     * 
     * For example: the following code will create a filter that filters 
     * out all files except those that end in ".jpg" and ".tif": 
     * 
     *   ExampleFileFilter filter = new ExampleFileFilter(); 
     *   filter.addExtension("jpg"); 
     *   filter.addExtension("tif"); 
     * 
     * Note that the "." before the extension is not needed and will be ignored. 
     */  
    public void addExtension(String extension) {  
		if(filters == null) {  
		    filters = new Hashtable(5);  
		}  
		filters.put(extension.toLowerCase(), this);  
		fullDescription = null;  
    }  
  
    /** 
     * Returns the human readable description of this filter. For 
     * example: "JPEG and GIF Image Files (*.jpg, *.gif)" 

     */  
    public String getDescription() {  
		if(fullDescription == null) {  
		    if(description == null || isExtensionListInDescription()) {  
		 fullDescription = description==null ? "(" : description + " (";  
		// build the description from the extension list  
		Enumeration extensions = filters.keys();  
		if(extensions != null) {  
		    fullDescription += "." + (String) extensions.nextElement();  
		    while (extensions.hasMoreElements()) {  
		fullDescription += ", " + (String) extensions.nextElement();  
		    }  
		}  
		fullDescription += ")";  
		    } else {  
		fullDescription = description;  
		    }  
		}  
		return fullDescription;  
    }  
    /** 
     * Sets the human readable description of this filter. For 
     * example: filter.setDescription("Gif and JPG Images"); 
     * 
     */  
    public void setDescription(String description) {  
		this.description = description;  
		fullDescription = null;  
    }  
    /** 
     * Determines whether the extension list (.jpg, .gif, etc) should 
     * show up in the human readable description. 
     * 
     * Only relevent if a description was provided in the constructor 
     * or using setDescription(); 
     * 
     */  
    public void setExtensionListInDescription(boolean b) {  
		useExtensionsInDescription = b;  
		fullDescription = null;  
    }  
    /** 
     * Returns whether the extension list (.jpg, .gif, etc) should 
     * show up in the human readable description. 
     * 
     * Only relevent if a description was provided in the constructor 
     * or using setDescription(); 
     * 
     */  
    public boolean isExtensionListInDescription() {  
    	return useExtensionsInDescription;  
    }  
}
class About extends JFrame{
	private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
	
    public About() {
        initComponents();
        setVisible(false);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        setResizable(false);
    }
    
	private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        Icone Icones = new Icone();

        setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);

        jLabel1.setIcon( Icones.getIcone("home")); // NOI18N

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel2.setText("ProDown - Programa Formatador de Dados");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel3.setText("Versão");

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel4.setText("Compilação");
        jLabel5.setText(":");
        jLabel6.setText(":");
        jLabel7.setText(Cody.getInstancia().getVersaoDoSistema());
        jLabel8.setText(Cody.getInstancia().getVersaoCompilacao());

        jButton2.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jButton2.setText("Ajuda Online");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                    try {
                        desktop.browse(Cody.getInstancia().getLinkAjuda());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        
        jButton4.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jButton4.setText("Desenvolvedores");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                    try {
                        desktop.browse(Cody.getInstancia().getLinkDesenvolvedores());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        jLabel9.setText("SkyWalker 2013 -");
        jLabel9.setEnabled(false);

        jLabel10.setText(Integer.toString(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR)));
        jLabel10.setEnabled(false);

        jLabel11.setText("     Cody é o programa para desktop utilizando para você formatar as");

        jLabel12.setText("informações coletas, sobre as aptidões físicas e de saúde, de alunos com");

        jLabel13.setText("Síndrome de Down de sua Instituição de Ensino.");

        jLabel14.setText("     Para maiores informações entre em contado com Prodown através do");

        jLabel15.setText("site e seus colaboradores.");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addContainerGap(18, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 468, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 468, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 468, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 468, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 468, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton4))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(155, 155, 155)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jLabel2)
                        .addGap(39, 39, 39)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6)
                            .addComponent(jLabel8))))
                .addGap(18, 18, 18)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton4))
                .addGap(16, 16, 16))
        );

        pack();
    }
}
class Instituicao extends JFrame{
	private static final long serialVersionUID = 1L;
	private javax.swing.JLabel cnpj_instituicao;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel nome_instituicao;
    private javax.swing.JLabel tipo_instituicao;
    private Cody cody;
    
    public Instituicao() {
    	cody = Cody.getInstancia();
        initComponents();
        this.setVisible(false);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
    }
    
    public void tornarVisivel(boolean visible){
    	
    	nome_instituicao.setText(cody.getNomeInstituicao());
    	
    	tipo_instituicao.setText(cody.getTipoInstituicao());
    	
    	cnpj_instituicao.setText(cody.getCnpjInstituicao());
    	this.setVisible(visible);
    }
    
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        nome_instituicao = new javax.swing.JLabel();
        tipo_instituicao = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        cnpj_instituicao = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        setResizable(false);
        
        jLabel2.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel2.setText("ProDown - Instituição Cadastrada");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel3.setText("Nome");

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel4.setText("Tipo");

        jLabel5.setText(":");

        jLabel6.setText(":");

        nome_instituicao.setText("Nome");

        tipo_instituicao.setText("Tip");

        jLabel9.setText("SkyWalker 2013 -");
        jLabel9.setEnabled(false);

        jLabel10.setText(Integer.toString(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR)));
        jLabel10.setEnabled(false);

        jLabel11.setText("     As informações a respeito da Instituição após inseridas não podem");

        jLabel12.setText("ser alteradas.");

        jLabel16.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel16.setText("CNPJ");

        cnpj_instituicao.setText("CNPJ");

        jLabel18.setText(":");
        jButton2.getCursor();
		jButton2.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jButton2.setText("Ajuda Online");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                    try {
                        desktop.browse(Cody.getInstancia().getLinkAjuda());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            
        });
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(nome_instituicao, javax.swing.GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE)
                            .addComponent(tipo_instituicao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cnpj_instituicao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 468, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 468, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(155, 155, 155)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(169, 169, 169)
                .addComponent(jButton2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jLabel2)
                        .addGap(39, 39, 39)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5)
                            .addComponent(nome_instituicao))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6)
                            .addComponent(tipo_instituicao))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(cnpj_instituicao)
                            .addComponent(jLabel18))))
                .addGap(18, 18, 18)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }
}