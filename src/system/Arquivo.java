/**Classe responsável pelo carregamento e inserção dos dados no arquivo.cody, cujo endereço esta em @path
 * @author Equipe DSskywalker
 */

package system;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.jdom2.Element;
import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

public class Arquivo {
	
	public String path;
	
	public Arquivo(){
	}

	/**
	 * Construtor da classe.
	 * @param path Caminho para o arquivo com terminação .cody
	 */
	public Arquivo(String path){
		this.path = path;

	}
	
	/**
	 * Método responsável pelo carregamento do arquivo .cody.
	 * @return	Retorna a tag root do documento carregado.
	 */
	public Element importarArquivoXML(){
		Document doc = null;
        SAXBuilder builder = new SAXBuilder();
        try {

              doc = builder.build(path);

        } catch (Exception e) {
        	
              e.printStackTrace();

        } 
        Element raiz = doc.getRootElement();
        return raiz;
        
	}
	

	/**
	 * Método responsável pela escrita no arquivo .cody.
	 * @param doc	Dados a serem salvos no xml
	 * @return		verdadeiro se conseguiu escrever, falso c.c.	
	 */
	public boolean exportarArquivoXML(Document doc){
		XMLOutputter xout = new XMLOutputter();
		try {
	        FileWriter arquivo = new FileWriter(new File(path));
	        xout.output(doc, arquivo);
	        return true;
		} catch (IOException e) {
	        return false;
		}		
	}
}
