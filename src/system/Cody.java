/***Classe responsável pela manipulação dos dados dentro do sistema offline(Cody) do projeto PRODOWN.
 * @author Equipe DSskywalker.
 */
package system;
import instituicao.Aluno;
import instituicao.Instituicao;
import instituicao.JTableModeloTeste;
import instituicao.Turma;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;

import config.Config;

public class Cody {
	private static volatile Cody instance = null;
	private JTableModeloTeste testesEsportivo;
	private JTableModeloTeste testesSaude;
	private JTableModeloTeste testesMedidas;
	private Instituicao instituicao;
	private Config config;
	private boolean temDadosParaSalvar;
    private String nomeArquivoAberto;
    private String versaoSistema;
    private String versaoCompilacao;
    private URI linkAjuda;
    private URI linkDesenvolvedores;
    
    /***
     * Construtor.
     */
    private Cody(){
    	config = Config.getInstancia();
    	this.instituicao = new Instituicao();
    	this.nomeArquivoAberto = null;
    	this.temDadosParaSalvar = false;
    	this.versaoSistema = "1.3";
    	this.versaoCompilacao = "32c7459";
    	
    	try {
			this.linkAjuda = new URI("http://kenobi.glaucomunsberg.com/ajuda");
		} catch (URISyntaxException e1) {
	
		}
    	try {
			this.linkDesenvolvedores = new URI("http://kenobi.glaucomunsberg.com/cody");
		} catch (URISyntaxException e) {
			
		}
    	
    }

    /***
     * @return		A única instÂncia do sistema Cody.
     */
    public static Cody getInstancia() {
        if (instance == null) {
        	synchronized (Config.class){
        		if (instance == null) {
        			instance = new Cody();
        		}
        	}
        }
        return instance;
    }
    
    public void resetAll(){
    	instituicao = new Instituicao();
    	this.setNaoHaDadosParaSalvar();
    	this.setCnpjInstituicao(null);
    }
    /***
     *@return 	A versão do sistema Cody.
     */
    public String getVersaoDoSistema(){
    	return this.versaoSistema;
    }
    
    public String getVersaoCompilacao(){
    	return this.versaoCompilacao;
    }
    
    public URI getLinkAjuda(){
    	return this.linkAjuda;
    }
    
    public URI getLinkDesenvolvedores(){
    	return this.linkDesenvolvedores;
    }

    /**
     *@param turmaId	Identificador da turma.
     *@return 			Os alunos da turma passada por parametro.
     */
    public String[] getListaAlunosByTurmaId(int turmaId){
		return instituicao.getTurma(turmaId).getAlunos();
	}
    

    /**
     *@return 		Todas as turmas da instituicao.
     */
	public String[] getListaTurmas(){
		return instituicao.getTurmas();
	}
	
	/**
     * Adiciona um novo aluno em uma turma.
     * @param turmaId			Identificador da turma.
     * @param nome				Nome do aluno.
     * @param dataNascimento 	Data de nascimento do aluno.
     * @param nomeMae			Nome da mae do aluno.
     * @param nomePai			Nome do pai do aluno.
     * @param genero			Genero do aluno.
     * @param cpf				Cadastro de pessoa fisica do aluno.
     * @param rg				Registro Geral do aluno.
     * @param endereco			Endereço do aluno.
     */
	public boolean setNovoAluno(int turmaId, String nome, String dataNascimento, String nomeMae, String nomePai, String genero, String cpf, String rg, String endereco){
		boolean retorno = instituicao.getTurma(turmaId).criarNovoAluno(nome, dataNascimento, nomeMae, nomePai, endereco, rg, cpf, genero);
		if(retorno){
			this.setHaDadosParaSalvar();
			return true;
		}else{
			return false;
		}
	}
	
	/**
     * Edita um aluno de uma turma.
     * @param turmaId			Identificador da turma.
     * @param alunoId			Identificado do aluno.
     * @param nome				Nome do aluno.
     * @param dataNascimento 	Data de nascimento do aluno.
     * @param nomeMae			Nome da mae do aluno.
     * @param nomePai			Nome do pai do aluno.
     * @param genero			Genero do aluno.
     * @param cpf				Cadastro de pessoa fisica do aluno.
     * @param rg				Registro Geral do aluno.
     * @param endereco			Endereço do aluno.
     */
	public boolean setEditarAluno(int turmaId, int alunoId, String nome, String dataNascimento, String nomeMae, String nomePai, String genero, String cpf, String rg, String endereco){
		int id = -1;
		for(Aluno aluno: instituicao.getTurma(turmaId).getListaAlunos()){
			id++;
			if(aluno.getNome().equals(nome) && aluno.getNomeMae().equals(nomeMae) && aluno.getDataNascimento().equals(dataNascimento) && id != alunoId){
				return false;
			}
		}
		instituicao.getTurma(turmaId).getAluno(alunoId).setNome(nome);
		instituicao.getTurma(turmaId).getAluno(alunoId).setDataNascimento(dataNascimento);
		instituicao.getTurma(turmaId).getAluno(alunoId).setNomeMae(nomeMae);
		instituicao.getTurma(turmaId).getAluno(alunoId).setNomePai(nomePai);
		instituicao.getTurma(turmaId).getAluno(alunoId).setEndereco(endereco);
		instituicao.getTurma(turmaId).getAluno(alunoId).setGenero(genero);
		instituicao.getTurma(turmaId).getAluno(alunoId).setCadPessoaFisica(cpf);
		instituicao.getTurma(turmaId).getAluno(alunoId).setRegistroGeral(rg);
		this.setHaDadosParaSalvar();
		return true;
	}
	

	/**
     * Adiciona uma nova turma.
     * @param nome				Nome da turma.
     * @param ano				Ano de ocorrência da turma.

     */
	public boolean setNovaTurma(String nome, String ano){
		config.setNovaMensagemNoLog("nova turma: "+ano+nome);
		return instituicao.criarNovaTurma(nome, ano);
	}
	

	/**
     * @return 			O nome da instituicao.
     */
	public String getNomeInstituicao(){
		return instituicao.getNome();
	}
	
	/** Importa os dados para dentro da instancia de instituicao.
     * @param path		Endereço do arquivo com terminação cody.
     * @return 			Verdadeiro caso o arquivo tenha terminação .cody, falso c.c.
     */
	public boolean importarArquivo(String path){
		if(path.substring(path.length()-5).equals(".cody")){
			
			Arquivo abrirArquivo = new Arquivo(path);
			this.setArquivoAberto(path);
			Element raiz = abrirArquivo.importarArquivoXML();
			
			List<Element> listaDeFilhosRaiz =  raiz.getChildren();
			String nomeInst, cnpjInst, tipoInst;
			
			Element nomeInstituicao = listaDeFilhosRaiz.get(0);
			nomeInst = nomeInstituicao.getText();
			listaDeFilhosRaiz.remove(0);
			Element cnpjInstituicao = listaDeFilhosRaiz.get(0);
			cnpjInst = cnpjInstituicao.getText();
			listaDeFilhosRaiz.remove(0);
	
			Element tipoInstituicao = listaDeFilhosRaiz.get(0);
			tipoInst = tipoInstituicao.getText();
			listaDeFilhosRaiz.remove(0);
			
			instituicao = new Instituicao(nomeInst, cnpjInst, tipoInst);
			instituicao.setTurmasXML(listaDeFilhosRaiz);
			
			return true;
		}
		else{
			return false;
		}
	}
	

	/** Exporta os dados para dentro do arquivo cody.
     * @param path		Endereço do arquivo com terminação cody.
     * @return 			Verdadeiro caso consiga inserir no arquivo, falso c.c.
     */
	public boolean exportarArquivo(String path){	
		config.setNovaMensagemNoLog("exportarArquivo: "+path);
		this.setArquivoAberto(path);
		
		Document doc;
		doc = instituicao.getInstituicaoXML();
		
		Arquivo SalvarArquivo = new Arquivo(path);
		return SalvarArquivo.exportarArquivoXML(doc);
	}
	
	/** 
     * @return 			Cadastro nacional de pessoa jurídica da instituicao.
     */
	public String getCnpjInstituicao(){
		return instituicao.getCnpj();
	}
	
	/** 
     * @return 			Hash do estado interno da classe.
     */
	public String getHashDoArquivo(){
		return Integer.toString(instituicao.hashCode());
	}
	/** 
     * @return 			Tipo de ensino da instituicao.
     */
	public String getTipoInstituicao(){
		return instituicao.getTipo();
	}
	
	/** Atualiza nome da instituicao.
	 * @param nome 		Nome da instituicao.
     */
	public void setNomeInstituicao(String nome){
		instituicao.setNome(nome);
		this.setHaDadosParaSalvar();
	}
	
	/** Atualiza cnpj da instituicao.
	 * @param cnpj		Cadastro nacional de pessoa juridica da instituicao.
     */
	public void setCnpjInstituicao(String cnpj){
		instituicao.setCnpj(cnpj);
		this.setHaDadosParaSalvar();
	}
	
	/** Atualiza tipo da instituicao.
	 * @param tipo		Tipo de ensino da instituicao.
     */
	public void setTipoInstituicao(String tipo){
		instituicao.setTipo(tipo);
		this.setHaDadosParaSalvar();
	}	
	
	/** Atualiza a flag de informação, de novos dados inseridos, para Verdadeiro.
     */
	public void setHaDadosParaSalvar(){
    	this.temDadosParaSalvar = true;
    }
    
	/** Atualiza a flag de informação, de novos dados inseridos, para Falso.
     */
    public void setNaoHaDadosParaSalvar(){
    	this.temDadosParaSalvar = false;
    }
    
    /**@return		 O valor que há dentro da flag de informação sobre novos dados inseridos.
     */
    public boolean temDadosParaSalvar(){
    	return this.temDadosParaSalvar;
    }
    
    /** Atualiza a flag de informação do nome do arquivo que está aberto.
     *@param arquivoAberto		 Nome do arquivo aberto.
     */
    public void setArquivoAberto(String arquivoAberto){
    	this.nomeArquivoAberto = arquivoAberto;
    }
    
    /**@return		Verdadeiro caso tenha algum arquivo aberto, falso c.c.
     */
    public boolean temArquivoAberto(){
    	if(this.nomeArquivoAberto == null){
    		return false;
    	}
    	return true;
    }

    /**@return		Numero de turmas.
     */
	public int getNumeroDeTurmas() {
		return instituicao.getNumeroDeTurmas();
	}

	/**@param turmaId	Identificador da turma.
	 *@return			Nome da turma passada por parametro.
     */
	public String getNomeTurmaById(int turmaId) {
		return instituicao.getTurma(turmaId).getNome();
	}

	/**
	 * Excluir
	 */
	public boolean removerTurmaById(int turmaIdSelecionado) {
		instituicao.excluirTurma(String.valueOf(turmaIdSelecionado));
		this.setHaDadosParaSalvar();
		return true;
	}

	public boolean setEditarTurmaById(int turmaIdSelecionado, String novoNome, String novoAno) {
		int id = -1;
		for(Turma turma: instituicao.getListaTurmas()){
			id++;
			if(turma.getNome().equals(novoNome) && turma.getAno().equals(novoAno) && id != turmaIdSelecionado){
				return false;
			}
		}
		instituicao.getTurma(turmaIdSelecionado).setNome(novoNome);
		instituicao.getTurma(turmaIdSelecionado).setAno(novoAno);
		this.setHaDadosParaSalvar();
		return true;
	}

	public String[] getListaTestesByTurma(int turmaId) {
		return instituicao.getTurma(turmaId).getTestes();
	}

	public boolean setNovoTesteByTurmaId(int turmaIdSelecionado, String nomeDoTest, String dataDoTest) {
		boolean retorno = instituicao.getTurma(turmaIdSelecionado).criarNovoTeste(dataDoTest, nomeDoTest);
		this.setHaDadosParaSalvar();
		return retorno;
	}

	public Aluno getAlunoIdByTurmaId(int turmaIdSelecionado, int alunoIdSelecionado) {
		return instituicao.getTurma(turmaIdSelecionado).getAluno(alunoIdSelecionado);
	}

	public boolean removeAlunoByTurma(int turmaIdSelecionado,int alunoIdSelecionado) {
		instituicao.getTurma(turmaIdSelecionado).excluirAluno(String.valueOf(alunoIdSelecionado));
		this.setHaDadosParaSalvar();
		return true;
	}
	
	private String formataString(String val){
		val = val.replace('.',' ');
		val = val.replace('-',' ');
        val = val.replace('/',' ');
        val = val.replace(':',' ');
        val = val.replaceAll(" ","");
        return val;
	}
	
	public boolean validaDataNasc(String data){
		GregorianCalendar calendar =  new GregorianCalendar();     
	       
        Calendar cal = Calendar.getInstance();
        int dia = 0,mes = 0,ano = 0;  
        int year = cal.get(Calendar.YEAR);  
        data = formataString(data);
        String diaStr = data.substring(0,2);  
        String mesStr = data.substring(2,4);  
        String anoStr = data.substring(4,8); 
       
        try {  
            dia = Integer.parseInt(diaStr);  
            mes = Integer.parseInt(mesStr);  
            ano = Integer.parseInt(anoStr);  
        } catch (Exception e) {  
            return false;  
        }  
        if(!(dia < 1 || mes < 1 || ano < 1)){
        	if(!((ano + 20)<(year)|| (year - 10) <(ano))){
        		if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
        			if(dia <= 31){
        				return true;
        			}
                }
        		else if(mes == 4 || mes == 6 || mes == 9 || mes == 11){
                    if (dia <= 30){
                    	return true;
                    }
        		}
                else if (mes == 2){
                	if (calendar.isLeapYear(ano))  {
                		if (dia <= 29)  {
                			return true;
                		}
                	}
                	else{
                		if (dia <= 28){
                			return true;
                		}
                	}
                }
        	}
        }
        return false; 
	}
	
	public boolean validarCpf(String cpf){
		int[] num ;
		int resultP = 0;  
		int resultS = 0;  
		cpf = formataString(cpf);
		num = new int[cpf.length()];
 
		for (int i = 0; i < num.length; i++) {  
			num[i] = Integer.parseInt(cpf.substring(i, i + 1));  
		}  

		for (int i = 0; i < 9; i++) { 
			resultP += num[i] * (i + 1);  
		} 
		int divP = resultP % 11;  
 
		if (divP != num[9]) {  
			return false;  
		}
		else {  

			for (int i = 0; i < 10; i++) {
				resultS += num[i] * (i);  
			}  
			int divS = resultS % 11;  
			if (divS != num[10]) {  
				return false;  
			}  
		}  
	  
		return true; 
	}
	
	public boolean validaRG(String strRG){
		if(strRG.length() <= 21 ){
			
			return strRG.matches("[0-9]*$");	
		}
		return false;	
	}
	
	public  boolean validaCNPJ(String strCNPJ) {
	    int iSoma = 0, iDigito;
	    char[] chCaracteresCNPJ;
	    String strCNPJ_Calculado;
	    strCNPJ=formataString(strCNPJ);
	    if (! strCNPJ.substring(0,1).equals("")){
	        try{
	            strCNPJ_Calculado = strCNPJ.substring(0,12);
	            if ( strCNPJ.length() != 14 ){
					 return false;
				 }
	            chCaracteresCNPJ = strCNPJ.toCharArray();
	            for(int i = 0; i < 4; i++) {
	                if ((chCaracteresCNPJ[i]-48 >= 0) && (chCaracteresCNPJ[i]-48 <= 9)) {
	                    iSoma += (chCaracteresCNPJ[i] - 48 ) * (6 - (i + 1)) ;
	                }
	            }
	            for( int i = 0; i < 8; i++ ) {
	                if ((chCaracteresCNPJ[i+4]-48 >= 0) && (chCaracteresCNPJ[i+4]-48 <= 9)) {
	                    iSoma += (chCaracteresCNPJ[i+4] - 48 ) * (10 - (i + 1)) ;
	                }
	            }
	            iDigito = 11 - (iSoma % 11);
	            strCNPJ_Calculado += ((iDigito == 10) || (iDigito == 11)) ? "0" : Integer.toString(iDigito);

	            iSoma = 0;
	            for (int i = 0; i < 5; i++) {
	                if ((chCaracteresCNPJ[i]-48 >= 0) && (chCaracteresCNPJ[i]-48 <= 9)) {
	                    iSoma += (chCaracteresCNPJ[i] - 48) * (7 - (i + 1)) ;
	                }
	            }
	            for (int i = 0; i < 8; i++) {
	                if ((chCaracteresCNPJ[i+5]-48 >= 0) && (chCaracteresCNPJ[i+5]-48 <= 9)) {
	                    iSoma += (chCaracteresCNPJ[i+5] - 48) * (10 - (i + 1)) ;
	                }
	            }
	            iDigito = 11 - (iSoma % 11);
	            strCNPJ_Calculado += ((iDigito == 10) || (iDigito == 11)) ? "0" : Integer.toString(iDigito);
	            return strCNPJ.equals(strCNPJ_Calculado);
	        } catch (Exception e) {
	            return false;
	        }
	    } else return false;
	}

	public String getArquivoAberto() {
		return this.nomeArquivoAberto;
	}
	
	@SuppressWarnings({ "rawtypes" })
	public JTableModeloTeste getTestesAptidaoEsportiva(int turmaId, int testeId){
		
		Object[] listaNomeColunas = new String[7];
		listaNomeColunas[0] = "Nome Aluno";
		listaNomeColunas[1] = "Arremesso de Medicineball (cm)";
		listaNomeColunas[2] = "Corrida de 20 metros (s)";
		listaNomeColunas[3] = "Quadrado (s)";
		listaNomeColunas[4] = "Salto Horizontal (cm)";
		listaNomeColunas[5] = "Desempenho Motor (6 min)";
		listaNomeColunas[6] = "Foram 9 min?";
		
		boolean[] listaColunaPodeEditar = new boolean[7];
		listaColunaPodeEditar[0] = false;
		listaColunaPodeEditar[1] = true;
		listaColunaPodeEditar[2] = true;
		listaColunaPodeEditar[3] = true;
		listaColunaPodeEditar[4] = true;
		listaColunaPodeEditar[5] = true;
		listaColunaPodeEditar[6] = true;
		
		Class[] tipoDoCampo = new Class[7];
		tipoDoCampo[0] = String.class;
		tipoDoCampo[1] = String.class;
		tipoDoCampo[2] = String.class;
		tipoDoCampo[3] = String.class;
		tipoDoCampo[4] = String.class;
		tipoDoCampo[5] = String.class;
		tipoDoCampo[6] = java.lang.Boolean.class;
		
		Object valoresColunas[][] = new Object[instituicao.getTurma(turmaId).getNumeroDeAlunos()][7];
		for(int i = 0; i < instituicao.getTurma(turmaId).getNumeroDeAlunos(); i++){
			Aluno aluno = instituicao.getTurma(turmaId).getAluno(i);
				valoresColunas[i][0] = aluno.getNome();
				valoresColunas[i][1] = aluno.getTesteAptEsp(testeId).getArremessoDeMedicineball();
				valoresColunas[i][2] = aluno.getTesteAptEsp(testeId).getCorridaVinteMetros();
				valoresColunas[i][3] = aluno.getTesteAptEsp(testeId).getQuadrado();
				valoresColunas[i][4] = aluno.getTesteAptEsp(testeId).getSaltoEmDistancia();
				valoresColunas[i][5] = aluno.getTesteAptEsp(testeId).getSeisMinutos();
				if(aluno.getTesteAptEsp(testeId).getNoveMinutos()){
					valoresColunas[i][6] = new Boolean(true) ;
				}else{
					valoresColunas[i][6] = new Boolean(false) ;
				}
					
		}
		
		testesEsportivo = new JTableModeloTeste(valoresColunas,listaNomeColunas,listaColunaPodeEditar,tipoDoCampo);
		return testesEsportivo;
	}
	
	@SuppressWarnings("rawtypes")
	public JTableModeloTeste getTestesAptidaoSaude(int turmaId, int testeId){
		Object[] listaNomeColunas = new String[5];
		listaNomeColunas[0] = "Nome do Aluno";
		listaNomeColunas[1] = "Abdominal (qtde)";
		listaNomeColunas[2] = "IMC";
		listaNomeColunas[3] = "Desempenho Motor em 6min (cm)";
		listaNomeColunas[4] = "Desempenho Motor de 9 min?";
				
		boolean[] listaColunaPodeEditar = new boolean[5];
		listaColunaPodeEditar[0] = false;
		listaColunaPodeEditar[1] = true;
		listaColunaPodeEditar[2] = true;
		listaColunaPodeEditar[3] = true;
		listaColunaPodeEditar[4] = true;
		
		Class[] tipoDoCampo = new Class[5];
		tipoDoCampo[0] = String.class;
		tipoDoCampo[1] = String.class;
		tipoDoCampo[2] = String.class;
		tipoDoCampo[3] = String.class;
		tipoDoCampo[4] = java.lang.Boolean.class;
		
		Object valoresColunas[][] = new Object[instituicao.getTurma(turmaId).getNumeroDeAlunos()][7];
		for(int i = 0; i < instituicao.getTurma(turmaId).getNumeroDeAlunos(); i++){
			Aluno aluno = instituicao.getTurma(turmaId).getAluno(i);
				valoresColunas[i][0] = aluno.getNome();
				valoresColunas[i][1] = aluno.getTesteAptSaude(testeId).getNumAbdominais();
				valoresColunas[i][2] = aluno.getTesteAptSaude(testeId).getImc();
				valoresColunas[i][3] = aluno.getTesteAptSaude(testeId).getSeisMinutos();
				if(aluno.getTesteAptSaude(testeId).getNoveMinutos()){
					valoresColunas[i][4] = true ;
				}else{
					valoresColunas[i][4] = false;
				}
				
		}
		
		this.testesSaude = new JTableModeloTeste(valoresColunas,listaNomeColunas,listaColunaPodeEditar,tipoDoCampo);
		return testesSaude;
	}
	
	@SuppressWarnings("rawtypes")
	public JTableModeloTeste getTestesMedidaCrescimento(int turmaId, int testeId){
		Object[] listaNomeColunas = new String[4];
		listaNomeColunas[0] = "Nome Aluno";
		listaNomeColunas[1] = "Massa Corporal (Kg)";
		listaNomeColunas[2] = "Estatura (cm)";
		listaNomeColunas[3] = "Envergadura (cm)";
		
		boolean[] listaColunaPodeEditar = new boolean[4];
		listaColunaPodeEditar[0] = false;
		listaColunaPodeEditar[1] = true;
		listaColunaPodeEditar[2] = true;
		listaColunaPodeEditar[3] = true;
		
		Class[] tipoDoCampo = new Class[4];
		tipoDoCampo[0] = String.class;
		tipoDoCampo[1] = String.class;
		tipoDoCampo[2] = String.class;
		tipoDoCampo[3] = String.class;
		
		String valoresColunas[][] = new String[instituicao.getTurma(turmaId).getNumeroDeAlunos()][4];
		for(int i = 0; i < instituicao.getTurma(turmaId).getNumeroDeAlunos(); i++){
			Aluno aluno = instituicao.getTurma(turmaId).getAluno(i);
				valoresColunas[i][0] = aluno.getNome();
				valoresColunas[i][1] = aluno.getMedidaCrecimento(testeId).getMassaCorporal();
				valoresColunas[i][2] = aluno.getMedidaCrecimento(testeId).getEstatura();
				valoresColunas[i][3] = aluno.getMedidaCrecimento(testeId).getEnvergadura();
		}
		
		this.testesMedidas = new JTableModeloTeste(valoresColunas,listaNomeColunas,listaColunaPodeEditar,tipoDoCampo);
		return this.testesMedidas;
	}
	
	public boolean setTestesAptidaoSaude(int turmaId, int testeId,JTableModeloTeste teste ){
		String values[] = new String[7];
		boolean noveMinutos = false;
		for(int i = 0; i < teste.getRowCount(); i++){
			for(int j = 0; j < teste.getColumnCount(); j++){
				
				if( j == 4){
					if((Boolean) teste.getValueAt(i, j) == false){
						noveMinutos = false;
					}else{
						noveMinutos = true;
					}
				}else{
					values[j] = String.valueOf(teste.getValueAt(i, j));
				}
			}
			
			
			instituicao.getTurma(turmaId).getAluno(i).editarTesteAptSaude(testeId, values[1],values[2], noveMinutos, values[3]);
		}
		return true;
	}
	
	public boolean setTestesAptidaoEsportiva(int turmaId, int testeId,JTableModeloTeste teste ){
		String values[] = new String[7];
		boolean eNoveMinuto = false;
		for(int i = 0; i < teste.getRowCount(); i++){
			for(int j = 0; j < teste.getColumnCount(); j++){
				if(j == 6){
					if((Boolean)teste.getValueAt(i, j) == false){
						eNoveMinuto = false;
					}else{
						eNoveMinuto = true;
					}
					
				}else{
					values[j] = String.valueOf(teste.getValueAt(i, j));
				}
				
			}
			instituicao.getTurma(turmaId).getAluno(i).editarTesteAptEsportivo(testeId, values[1], values[2], values[3], values[4], eNoveMinuto, values[5]);
		}
		return true;
	}
	
	public boolean setTestesMedidaCrescimento(int turmaId, int testeId,JTableModeloTeste teste ){
		String values[] = new String[4];
		for(int i = 0; i < teste.getRowCount(); i++){
			for(int j = 0; j < teste.getColumnCount(); j++){
				values[j] = String.valueOf(teste.getValueAt(i, j));
			}
			instituicao.getTurma(turmaId).getAluno(i).editarTesteMedidaCrescimento(testeId, values[1], values[2], values[3]);
		}
		return true;
	}
	
	public boolean setExcluirTesteTurma(int turmaId, int testeId){
		instituicao.getTurma(turmaId).excluirTeste(testeId);
		return true;
	}

	public String getTurmaAnoById(int turmaIdSelecionado) {
		return instituicao.getTurma(turmaIdSelecionado).getAno();
	}
	
}
	

