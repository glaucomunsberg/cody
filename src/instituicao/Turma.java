package instituicao;

import java.util.ArrayList;
import java.util.List;

import org.jdom2.Element;

/**Classe turma contem os dados de uma turma( nome e ano de ocorrência), além da lista de alunos e da lista de testes relacionados a esta turma.
 * 
 * @author Equipe DSSkywalker.
 *
 */
public class Turma {
	
	private String nome;
	private String ano;
	private ArrayList<Aluno> alunos;
	private ArrayList<String> testes;
	
	public Turma(){
		alunos = new ArrayList<Aluno>();
		testes = new ArrayList<String>();
	}
	
	public Turma(String nome, String ano){
		this.nome = nome;
		this.ano = ano;
		alunos = new ArrayList<Aluno>();
		testes = new ArrayList<String>();
	}
	
	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public String getAno(){
		return ano;
	}
	
	public void setAno(String ano){
		this.ano = ano;
	}
	
	public String[] getAlunos(){
		String[] retornaAlunos = new String[ alunos.size() ];
		int a = 0;
		for(Aluno aluno : alunos){
			retornaAlunos[a] = aluno.getNome();
			a++;
		}
		return retornaAlunos;
	}
	
	public Aluno getAluno(int id){
		return alunos.get(id);
	}

	public void setAlunos(ArrayList<Aluno> alunos){
		this.alunos.clear();
		for(Aluno aluno : alunos){
			this.alunos.add(aluno);
		}
	}
	
	public ArrayList<Aluno> getListaAlunos(){
		return this.alunos;
	}
	
	public void excluirAluno(String identificador){
		alunos.remove(Integer.parseInt(identificador));
	}
	
	public boolean criarNovoAluno(String nome, String dataNascimento, String nomeMae, String nomePai, String endereco, String rg, String cpf, String genero){
		for(Aluno aluno:alunos){
			if(aluno.getNome().equals(nome) && aluno.getNomeMae().equals(nomeMae) && aluno.getDataNascimento().equals(dataNascimento)){
				return false;
			}
		}
		Aluno aluno = new Aluno(nome, dataNascimento, nomeMae, nomePai, endereco, rg, cpf, genero);
		for(String teste : testes){
			String nomeTeste = teste.substring(0, teste.length() - 11);
			String dataTeste = teste.substring(teste.length() - 10);
			aluno.criarMedidasCrescimento(dataTeste, nomeTeste);
			aluno.criarTesteAptEsportivo(dataTeste, nomeTeste);
			aluno.criarTesteAptSaude(dataTeste, nomeTeste);
		}
		alunos.add(aluno);
		return true;
	}
	
	public boolean criarNovoTeste(String data, String nome){
		String[] data_formatada = data.split("/");
		if( Integer.parseInt(data_formatada[2]) < 2000 || Integer.parseInt(data_formatada[1]) > 12 || Integer.parseInt(data_formatada[0]) > 31){
			return false;
		};
		String novo_teste = nome+" "+data;
		for(String teste: testes){
			if(novo_teste.equals(teste)){
				return false;
			}
		}
		for(Aluno aluno : alunos){
			aluno.criarMedidasCrescimento(data, nome);
			aluno.criarTesteAptEsportivo(data, nome);
			aluno.criarTesteAptSaude(data, nome);
		}
		testes.add(nome + " " + data);
		return true;
	}
	
	public void excluirTeste(int id){
		int i;
		for(i = 0; i< alunos.size(); i++){
			alunos.get(i).excluirTeste(id);
		}
		testes.remove(id);
	}
	
	public String[] getTestes(){
		String[] retornaTestes = new String[ testes.size() ];
		int a = 0;
		
		for(String teste : testes){
			retornaTestes[a] = teste;
			a++;
		}
		return retornaTestes;
	}
	
	public int getNumeroDeAlunos() {
		return alunos.size();
	}
	
	public boolean setTeste(String nome, String data){
		testes.add(nome + " " + data);
		return true;
	}
	
	public boolean importarTurmaXml(List<Element> alunosDaTurma){
		String nomeAluno, dataNascimento, nomeMae, nomePai, endereco, registroGeral, cadPessoaFisica, genero;
		List<Element> testesDoAluno = null;
		ArrayList<Aluno> alunosDoXml = new ArrayList<Aluno>();
		Aluno instAluno;
		
		for(Element alunoTag: alunosDaTurma){
			nomeAluno = alunoTag.getChildText("nome");
			dataNascimento = alunoTag.getChildText("dataNascimento");
			nomeMae = alunoTag.getChildText("nomeMae");
			nomePai = alunoTag.getChildText("nomePai");
			endereco = alunoTag.getChildText("endereco");
			registroGeral = alunoTag.getChildText("registroGeral");
			cadPessoaFisica = alunoTag.getChildText("cadPessoaFisica");
			genero = alunoTag.getChildText("genero");
			
			instAluno = new Aluno(nomeAluno,
					 dataNascimento,
					 nomeMae,
					 nomePai,
					 endereco,
					 registroGeral,
					 cadPessoaFisica,
					 genero);
		
			testesDoAluno = alunoTag.getChildren("teste");
			testes.clear();
			for(Element identificacaoTeste: testesDoAluno){
				setTeste(identificacaoTeste.getChildText("nome"), identificacaoTeste.getChildText("data"));
			}
			instAluno.importarAlunoXml(testesDoAluno);
			
			alunosDoXml.add(instAluno);
		}
		setAlunos(alunosDoXml);
		return true;
	}
	
	public Element exportarTurmaXml(){
		int i;
		Element turmaXML = new Element("turma");
		
		Element turmaNome = new Element("nome");
		turmaNome.setText(nome);
		
		Element turmaAno = new Element("ano");
		turmaAno.setText(ano);
		
		turmaXML.addContent(turmaNome);
		turmaXML.addContent(turmaAno);
		
		if(getNumeroDeAlunos() > 0){
			for(i=0;i<getNumeroDeAlunos();i++){
				Element alunoXML;
				alunoXML = alunos.get(i).exportarAlunoXML();
				turmaXML.addContent(alunoXML);
			}
		}
		return turmaXML;
	}
}
