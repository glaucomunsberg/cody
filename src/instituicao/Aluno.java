

package instituicao;

import config.Config;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Element;
/** Classe responsável pela descrição de um aluno e métodos que modificam este aluno. O aluno pode ter ligado a ele tres tipos de testes:
 * Aptidao saude, Aptidao esportivo e Medidas fisicas deste aluno.
 * 
 * @author Equipe DSskywalker
 *
 */
public class Aluno {
	
	private Config config;
	private String nome;
	private String dataNascimento;
	private String nomeMae;
	private String nomePai;
	private String endereco;
	private String registroGeral;
	private String cadPessoaFisica;
	private String genero;
	private ArrayList<TesteAptidaoSaude> testAptSaude;
	private ArrayList<TesteAptidaoEsportivo> testAptEsp;
	private ArrayList<MedidaCrescimento> medida;
	
	public Aluno(){
		config = Config.getInstancia();
		config.setNovaMensagemNoLog("Classe Aluno: Inicializada");
		testAptSaude = new ArrayList<TesteAptidaoSaude>();
		testAptEsp = new ArrayList<TesteAptidaoEsportivo>();
		medida = new ArrayList<MedidaCrescimento>();
	}
	
	public Aluno(String nome, String dataNascimento, String nomeMae, String nomePai, String endereco, String registroGeral, String cadPessoaFisica, String genero){
		config = Config.getInstancia();
		config.setNovaMensagemNoLog("Classe Aluno: Inicializada");
		testAptSaude = new ArrayList<TesteAptidaoSaude>();
		testAptEsp = new ArrayList<TesteAptidaoEsportivo>();
		medida = new ArrayList<MedidaCrescimento>();	
		
		this.nome = nome;
		this.dataNascimento = dataNascimento;
		this.nomeMae = nomeMae;
		this.nomePai = nomePai;
		this.endereco = endereco;
		this.registroGeral = registroGeral;
		this.cadPessoaFisica = cadPessoaFisica;
		this.genero = genero;
	}
	
	public String getNome(){
		return nome;
	}
	
	public String getDataNascimento(){
		return dataNascimento;
	}
	
	public String getNomeMae(){
		return nomeMae;
	}
	
	public String getNomePai(){
		return nomePai;
	}
	
	public String getEndereco(){
		return endereco;
	}
	
	public String getRegistroGeral(){
		return registroGeral;
	}
	
	public String getcadPessoaFisica(){
		return cadPessoaFisica;
	}
	
	public String getGenero(){
		return genero;
	}

	public void setNome(String nome){
		this.nome = nome;
	}
	
	public void setDataNascimento(String dataNascimento){
		this.dataNascimento = dataNascimento;
	}
	
	public void setNomeMae(String nomeMae){
		this.nomeMae = nomeMae;
	}
	
	public void setNomePai(String nomePai){
		this.nomePai = nomePai;
	}
	
	public void setEndereco(String endereco){
		this.endereco = endereco;
	}
	
	public void setRegistroGeral(String registroGeral){
		this.registroGeral = registroGeral;
	}
	
	public void setCadPessoaFisica(String cadPessoaFisica){
		this.cadPessoaFisica = cadPessoaFisica;
	}
	
	public void setGenero(String genero){
		this.genero = genero;
	}
	
	public void excluirTeste(int data){
		testAptSaude.remove(data);
		testAptEsp.remove(data);
		medida.remove(data);
	}

/** Cria o Teste Aptidao Saude do aluno.
	* 
	* @param data			Data de ocorrência do teste.
	* @param nome			Nome do teste.
*/
	public void criarTesteAptSaude(String data, String nome){
		TesteAptidaoSaude teste = new TesteAptidaoSaude(data, nome);
		testAptSaude.add(teste);
	}

/** Cria o Teste Aptidao Esportivo do aluno.
	* 
	* @param data			Data de ocorrência do teste.
	* @param nome			Nome do teste.
*/
	public void criarTesteAptEsportivo(String data, String nome){
		TesteAptidaoEsportivo teste = new TesteAptidaoEsportivo(data, nome);
		testAptEsp.add(teste);
	}
	
/** Cria o teste de medidas de crescimento do aluno.
	* 
	* @param data			Data de ocorrência do teste.
	* @param nome			Nome do teste.
*/
	public void criarMedidasCrescimento(String data, String nome){
		MedidaCrescimento teste = new MedidaCrescimento(data, nome);
		medida.add(teste);
	}

	public void editarTesteAptEsportivo(int id, String abdominal, String imc, String sentarAlcancar, String sentarAlcancarSemBanco, boolean noveMinutos, String seisMinutos){
		testAptEsp.get(id).setCorridaVinteMetros(imc);
		testAptEsp.get(id).setArremessoDeMedicineball(abdominal);
		testAptEsp.get(id).setQuadrado(sentarAlcancar);
		testAptEsp.get(id).setSaltoEmDistancia(sentarAlcancarSemBanco);
		testAptEsp.get(id).setNoveMinutos(noveMinutos);
		testAptEsp.get(id).setSeisMinutos(seisMinutos);
	}
	
	public void editarTesteAptSaude(int id, String arremessoDeMediceball, String corridaVinteMetros, boolean noveMinutos, String seisMinutos){
		testAptSaude.get(id).setNumAbdominais(arremessoDeMediceball);
		testAptSaude.get(id).setImc(corridaVinteMetros);
		testAptSaude.get(id).setNoveMinutos(noveMinutos);
		testAptSaude.get(id).setSeisMinutos(seisMinutos);
	}
	
	public void editarTesteMedidaCrescimento(int id, String massaCorporal, String estatura, String envergadura){
		medida.get(id).setMassaCorporal(massaCorporal);
		medida.get(id).setEstatura(estatura);
		medida.get(id).setEnvergadura(envergadura);
	}
	
	public int getNumeroDeTestesAptidaoSaude() {
		return testAptSaude.size();
	}
	
	public int getNumeroDeTestesAptidaoEsportivo() {
		return testAptEsp.size();
	}
	
	public int getNumeroDeTestesMedidaCrescimento() {
		return medida.size();
	}
	
	public TesteAptidaoSaude getTesteAptSaude(int id){
		return testAptSaude.get(id);
	
	}
	
	public TesteAptidaoEsportivo getTesteAptEsp(int id){
		return testAptEsp.get(id);
	}
	
	public MedidaCrescimento getMedidaCrecimento(int id){
		return medida.get(id);
	}
	
	public String[] getTestesAptidaoSaude() {
		String[] retornaTesteAptidaoSaude = new String[ testAptSaude.size() ];
		int a = 0;
		for(TesteAptidaoSaude teste : testAptSaude){
			retornaTesteAptidaoSaude[a] = teste.getData();
			a++;
		}
		return retornaTesteAptidaoSaude;
	}
	
	public String[] getTestesAptidaoEsportivo() {
		String[] retornaTesteAptidaoEsportivo = new String[ testAptEsp.size() ];
		int a = 0;
		for(TesteAptidaoEsportivo teste : testAptEsp){
			retornaTesteAptidaoEsportivo[a] = teste.getData();
			a++;
		}
		return retornaTesteAptidaoEsportivo;
	}
	
	public String[] getTestesMedidaCrescimento() {
		String[] retornaTestesMedidaCrescimento = new String[ medida.size() ];
		int a = 0;
		for(MedidaCrescimento teste : medida){
			retornaTestesMedidaCrescimento[a] = teste.getData();
			a++;
		}
		return retornaTestesMedidaCrescimento;
	}
	
public boolean importarAlunoXml(List<Element> listaTestesAlunos){
		//Teste Aptidao Esportivo
		boolean noveMinutosTAE,noveMinutosTAS = false;
		String abdominal, imc, seisMinutosTAE;
		//Medida de crescimento
		String massaCorporal, estatura, envergadura; 									 
		// Teste Aptidao Saude
		String arremessoDeMedicineball, corridaVinteMetros, quadrado, saltoEmDistancia, seisMinutosTAS;
		String data, nomeTeste;
		
		TesteAptidaoSaude instTAS;
		TesteAptidaoEsportivo instTAE;
		MedidaCrescimento instMC;
		
			
		for(Element testeTag: listaTestesAlunos){
			nomeTeste = testeTag.getChildText("nome");
			data = testeTag.getChildText("data");
			Element TAS = testeTag.getChild("testeAptidaoSaude");
			Element TAE = testeTag.getChild("testeAptidaoEsportivo");
			Element MC = testeTag.getChild("medidaCrescimento");
			
			
			abdominal = TAS.getChildText("numAbdominais");
			imc = TAS.getChildText("imc");
				
			if("true".equals(TAS.getChildText("noveMinutos"))){
				noveMinutosTAS = true;
			}else{
				noveMinutosTAS = false;
			}
			seisMinutosTAS = TAS.getChildText("seisMinutos");
				
			instTAS = new TesteAptidaoSaude(data, nomeTeste, abdominal, imc, noveMinutosTAS, seisMinutosTAS );
			testAptSaude.add(instTAS);
				
		
			arremessoDeMedicineball = TAE.getChildText("arremessoDeMedicineball");
			corridaVinteMetros = TAE.getChildText("corridaVinteMetros");
			quadrado = TAE.getChildText("quadrado");
			saltoEmDistancia = TAE.getChildText("saltoEmDistancia");
			if("true".equals(TAE.getChildText("noveMinutos")) ){
				noveMinutosTAE = true;
			}else{
				noveMinutosTAE = false;
			}
			
			seisMinutosTAE = TAE.getChildText("seisMinutos");
					
			instTAE = new TesteAptidaoEsportivo(data, nomeTeste, arremessoDeMedicineball, corridaVinteMetros, quadrado, saltoEmDistancia, noveMinutosTAE, seisMinutosTAE);
			testAptEsp.add(instTAE);
				
			
			massaCorporal = MC.getChildText("massaCorporal");
			estatura = MC.getChildText("estatura");
			envergadura = MC.getChildText("envergadura");
					
			instMC = new MedidaCrescimento(data, nomeTeste, massaCorporal, estatura, envergadura);
			medida.add(instMC);
			
		}
			
		return true;
	}
		
		
	
	public Element exportarAlunoXML(){
		int i;
		Element alunoXML = new Element("aluno");
		
		Element alunoNome = new Element("nome");
		alunoNome.setText(nome);
		alunoXML.addContent(alunoNome);
		
		Element alunoDataNascimento = new Element("dataNascimento");
		alunoDataNascimento.setText(dataNascimento);
		alunoXML.addContent(alunoDataNascimento);
		
		if((nomeMae != null) && (!nomeMae.isEmpty()) 
		&& (nomeMae.length() != 0)){
			Element alunoNomeMae = new Element("nomeMae");
			alunoNomeMae.setText(nomeMae);
			alunoXML.addContent(alunoNomeMae);
		}
		
		if((nomePai != null) && (!nomePai.isEmpty()) 
		&& (nomePai.length() != 0)){
			Element alunoNomePai = new Element("nomePai");
			alunoNomePai.setText(nomePai);	
			alunoXML.addContent(alunoNomePai);			
		}
		
		if((endereco != null) && (!endereco.isEmpty()) && 
		(endereco.length() != 0)){
			Element alunoEndereco = new Element("endereco");
			alunoEndereco.setText(endereco);	
			alunoXML.addContent(alunoEndereco);
		}
		
		if((registroGeral != null) && (!registroGeral.isEmpty()) && 
		(registroGeral.length() != 0)){
			Element alunoRegistroGeral = new Element("registroGeral");
			alunoRegistroGeral.setText(registroGeral);
			alunoXML.addContent(alunoRegistroGeral);		
		}
		
		if((cadPessoaFisica != null) && (!cadPessoaFisica.isEmpty()) && 
		(nomeMae.length() != 0)){
			Element alunoCadPessoaFisica = new Element("cadPessoaFisica");
			alunoCadPessoaFisica.setText(cadPessoaFisica);
			alunoXML.addContent(alunoCadPessoaFisica);		
		}

		Element alunoGenero = new Element("genero");
		alunoGenero.setText(genero);
		alunoXML.addContent(alunoGenero);
		
		if((testAptSaude.size() == testAptEsp.size()) &&
		(testAptEsp.size() == medida.size())){			
			for(i=0;i<medida.size();i++){
				Element testeXML = new Element("teste");
				
				Element testeNome = new Element("nome");
				testeNome.setText(medida.get(i).getNome());
				testeXML.addContent(testeNome);
				
				Element testeData = new Element("data");
				testeData.setText(medida.get(i).getData());
				testeXML.addContent(testeData);
				
				Element testeAptSaudeXML;
				testeAptSaudeXML = testAptSaude.get(i).exportarTesteAptidaoSaudeXML();
				testeXML.addContent(testeAptSaudeXML);
				
				Element testeAptEsportivoXML;
				testeAptEsportivoXML = testAptEsp.get(i).exportarTesteAptidaoEsportivaXML();
				testeXML.addContent(testeAptEsportivoXML);
				
				Element testeMedidaCrescXML;
				testeMedidaCrescXML = medida.get(i).exportarMedidaCrescimentoXML();
				testeXML.addContent(testeMedidaCrescXML);

				alunoXML.addContent(testeXML);
			}
		}
		return alunoXML;
	}
}
