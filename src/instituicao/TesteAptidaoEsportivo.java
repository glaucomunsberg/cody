package instituicao;

import org.jdom2.Element;
/**CLasse Teste aptidao esportivo. Possui todos os dados que definem um teste de aptidao esportivo e os métodos que manipulam esses dados.
 * Um teste de aptidao esportivo contém os resultados de: o exercicio de arremesso de uma medicine ball, o exercicio de corrida 20 metros, 
 * o exercicio referente ao teste do quadrado, o exercicio do salto em distância e ainda o resultado do exercicio de corrida durante 6 ou 9 minutos.
 * @author Equipe DSSkywalker.
 *
 */
public class TesteAptidaoEsportivo extends Teste {
	
	private double arremessoDeMedicineball;
	private double corridaVinteMetros;
	private double quadrado;
	private double saltoEmDistancia;
	private double seisMinutos;
	private boolean noveMinutos;
	
	public TesteAptidaoEsportivo(){
		arremessoDeMedicineball = 0;
		corridaVinteMetros = 0;
		quadrado = 0;
		saltoEmDistancia = 0;
		seisMinutos = 0;
		noveMinutos = false;
	}
	
	public TesteAptidaoEsportivo(String data, String nome){
		super(data, nome);
		arremessoDeMedicineball = 0;
		corridaVinteMetros = 0;
		quadrado = 0;
		saltoEmDistancia = 0;
		seisMinutos = 0;
		noveMinutos = false;
	}
	
	public TesteAptidaoEsportivo(String data, String nome, String arremessoDeMedicineball, String corridaVinteMetros, String quadrado, String saltoEmDistancia, boolean noveMinutos, String seisMinutos){
		
		super(data, nome);
		
		try{
			this.arremessoDeMedicineball = Double.parseDouble(arremessoDeMedicineball);
		} catch (Exception e){
			this.arremessoDeMedicineball = 0;
		}
		try{
			this.corridaVinteMetros = Double.parseDouble(corridaVinteMetros);
		} catch (Exception e){
			this.corridaVinteMetros = 0;
		}
		try{
			this.quadrado = Double.parseDouble(quadrado);
		} catch (Exception e){
			this.quadrado = 0;
		}
		try{
			this.saltoEmDistancia = Double.parseDouble(saltoEmDistancia);
		} catch (Exception e){
			this.saltoEmDistancia = 0;
		}
		this.noveMinutos = noveMinutos;
		try{
			this.seisMinutos = Double.parseDouble(seisMinutos);
		} catch (Exception e){
			this.seisMinutos = 0;
		}	
	}


	
	public String getArremessoDeMedicineball(){
		return String.valueOf(arremessoDeMedicineball);
	}
	
	public String getCorridaVinteMetros(){
		return String.valueOf(corridaVinteMetros);
	}
	
	public String getQuadrado(){
		return String.valueOf(quadrado);
	}
	
	public String getSaltoEmDistancia(){
		return String.valueOf(saltoEmDistancia);
	}
	
	public boolean getNoveMinutos(){
		return this.noveMinutos;
	}
	
	public String getSeisMinutos(){
		return String.valueOf(seisMinutos);
	}
	
	public void setArremessoDeMedicineball(String arremessoDeMedicineball){
		try{
			this.arremessoDeMedicineball = Double.parseDouble(arremessoDeMedicineball);
		} catch (Exception e){
			try{
				this.arremessoDeMedicineball = Integer.parseInt(arremessoDeMedicineball);
			} catch (Exception a){
				this.arremessoDeMedicineball = 0;
			}
		}
	}
	
	public void setCorridaVinteMetros(String corridaVinteMetros){
		try{
			this.corridaVinteMetros = Double.parseDouble(corridaVinteMetros);
		} catch (Exception e){
			try{
				this.corridaVinteMetros = Integer.parseInt(corridaVinteMetros);
			} catch (Exception a){
				this.corridaVinteMetros = 0;
			}
		}
	}
	
	public void setQuadrado(String quadrado){
		try{
			this.quadrado = Double.parseDouble(quadrado);
		} catch (Exception e){
			try{
				this.quadrado = Integer.parseInt(quadrado);
			} catch (Exception a){
				this.quadrado = 0;
			}
		}
	}
	
	public void setSaltoEmDistancia(String saltoEmDistancia){
		try{
			this.saltoEmDistancia = Double.parseDouble(saltoEmDistancia);
		} catch (Exception e){
			try{
				this.saltoEmDistancia = Integer.parseInt(saltoEmDistancia);
			} catch (Exception a){
				this.saltoEmDistancia = 0;
			}
		}
	}
	
	public void setNoveMinutos(boolean noveMinutos){
		this.noveMinutos = noveMinutos;
	}
	
	public void setSeisMinutos(String seisMinutos){
		try{
			this.seisMinutos = Double.parseDouble(seisMinutos);
		} catch (Exception e){
			try{
				this.seisMinutos = Integer.parseInt(seisMinutos);
			} catch (Exception a){
				this.seisMinutos = 0;
			}
		}
	}
	
	public Element exportarTesteAptidaoEsportivaXML(){
		Element testeXML = new Element("testeAptidaoEsportivo");

		if((getArremessoDeMedicineball() != null) && (!getArremessoDeMedicineball().isEmpty()) 
		&& (getArremessoDeMedicineball().length() != 0)){		
			Element arremessoMedicineballXML = new Element("arremessoDeMedicineball");
			arremessoMedicineballXML.setText(getArremessoDeMedicineball());
			testeXML.addContent(arremessoMedicineballXML);
		}
	
		if((getCorridaVinteMetros() != null) && (!getCorridaVinteMetros().isEmpty()) 
		&& (getCorridaVinteMetros().length() != 0)){	
			Element corridaVinteMetrosXML = new Element("corridaVinteMetros");
			corridaVinteMetrosXML.setText(getCorridaVinteMetros());
			testeXML.addContent(corridaVinteMetrosXML);		
		}

		if((getQuadrado() != null) && (!getQuadrado().isEmpty()) 
		&& (getQuadrado().length() != 0)){
			Element quadradoXML = new Element("quadrado");
			quadradoXML.setText(getQuadrado());
			testeXML.addContent(quadradoXML);		
		}

		if((getSaltoEmDistancia() != null) && (!getSaltoEmDistancia().isEmpty()) 
		&& (getSaltoEmDistancia().length() != 0)){
			Element saltoEmDistanciaXML = new Element("saltoEmDistancia");
			saltoEmDistanciaXML.setText(getSaltoEmDistancia());
			testeXML.addContent(saltoEmDistanciaXML);		
		}

		if( getNoveMinutos() ){
			Element noveMinutosXML = new Element("noveMinutos");
			noveMinutosXML.setText("true");
			testeXML.addContent(noveMinutosXML);
		}else{
			Element noveMinutosXML = new Element("noveMinutos");
			noveMinutosXML.setText("false");
			testeXML.addContent(noveMinutosXML);
		}
		
		if((getSeisMinutos() != null) && (!getSeisMinutos().isEmpty())
		&& (getSeisMinutos().length() != 0)){
			Element seisMinutosXML = new Element("seisMinutos");
			seisMinutosXML.setText(getSeisMinutos());
			testeXML.addContent(seisMinutosXML);
		}
		
		return testeXML;	
	}
}
