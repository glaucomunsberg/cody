package instituicao;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**Classe utilizada para a apresentação dos dados na interface de testes. Atribuindo caixas de marcação aos testes de Aptidao saude e Aptidao esportivo.
 * 
 * @author Equipe DSSkywalker.
 */
public class JTableModeloTeste extends DefaultTableModel{

	private static final long serialVersionUID = 1L;
	private Object[][] linhas;
	private Object[] colunas;
	private boolean[] canEdit;
	@SuppressWarnings("rawtypes")
	private Class[] types;
	@SuppressWarnings("rawtypes")
	public JTableModeloTeste( Object[][] valoresColunas,Object[] listaNomeColunas, boolean[] podeEditar, Class[] tipoDoCampo){
		super( valoresColunas, listaNomeColunas);
		setPodeEditar(podeEditar);
		setTipoCampo(tipoDoCampo);
	}
	
	public JTableModeloTeste(TableModel tabela){
		
	}
	
	@SuppressWarnings("rawtypes")
	private void setTipoCampo(Class[] tipoDoCampo) {
		types = tipoDoCampo;
		
	}

	private void setPodeEditar(boolean[] podeEditar) {
		// TODO Auto-generated method stub
		canEdit = podeEditar;
	}

	public void setLinhas(Object[][] dados){

		linhas = dados;
	}

	public void setColunas(Object[] dados){

		colunas = dados;
	}
	
	
	public Object[] getLinha(int posi){
		return linhas[posi];
	}


	public Object getLinhas(){
		return linhas;
	}

	public Object[] getColunas(){

		return colunas;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }
	
}
