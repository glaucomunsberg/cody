package instituicao;

import org.jdom2.Element;

/**Classe Teste aptidao saude. Possui todos os dados que compõem um teste de aptidao saude, e os métodos que manipulam estes dados.
 * Um teste de aptidao saude contém, numero de abdominais, indice de massa corporal, o exercicio de sentar e alcançar( com ou sem banco), e ainda
 * o exercicio de corrida(Durante 6 ou 9 minutos).
 * @author Equipe DSSkywalker.
 *
 */
public class TesteAptidaoSaude extends Teste {
	
	private double numAbdominais;
	private double imc;
	private boolean sentarAlcancarComBanco;
	private double sentarAlcancar;
	private boolean noveMinutos;
	private double seisMinutos;
	
	public TesteAptidaoSaude(){
		numAbdominais = 0;
		imc = 0;
		sentarAlcancarComBanco = false;
		sentarAlcancar = 0;
		noveMinutos = false;
		seisMinutos = 0;
	}
	
	public TesteAptidaoSaude(String data, String nome){
		super(data, nome);
		numAbdominais = 0;
		imc = 0;
		sentarAlcancarComBanco = false;
		sentarAlcancar = 0;
		noveMinutos = false;
		seisMinutos = 0;
	}
	
	public TesteAptidaoSaude(String data, String nome, String numAbdominais, String imc, boolean noveMinutos, String seisMinutos){
		
		super(data, nome);
		
		try{
			this.numAbdominais = Double.parseDouble(numAbdominais);
		} catch (Exception e){
			this.numAbdominais = 0;
		}
		
		try{
			this.imc = Double.parseDouble(imc);
		} catch (Exception e){
			this.imc = 0;
		}
		
		this.noveMinutos = noveMinutos;
		
		try{
			this.seisMinutos = Double.parseDouble(seisMinutos);
		} catch (Exception e){
			this.seisMinutos = 0;
		}
	}
	
	public String getNumAbdominais(){
		return String.valueOf(numAbdominais);
	}
	
	public String getImc(){
		return String.valueOf(imc);
	}
	
	public boolean getSentarAlcancarComBanco(){
		return this.sentarAlcancarComBanco;
	}
	
	public String getsentarAlcancar(){
		return String.valueOf(this.sentarAlcancar);
	}
	
	public boolean getNoveMinutos(){
		return this.noveMinutos;
	}
	
	public String getSeisMinutos(){
		return String.valueOf(seisMinutos);
	}
	
	public void setNumAbdominais(String numAbdominais){
		try{
			this.numAbdominais = Double.parseDouble(numAbdominais);
		} catch (Exception e){
			try{
				this.numAbdominais = Integer.parseInt(numAbdominais);
			} catch (Exception a){
				this.numAbdominais = 0;
			}
		}
	}
	
	public void setImc(String imc){
		try{
			this.imc = Double.parseDouble(imc);
		} catch (Exception e){
			try{
				this.imc = Integer.parseInt(imc);	
			} catch (Exception a){
				this.imc = 0;
			}
		}
	}
	
	public void setSentarAlcancarComBanco(boolean sentarAlcancarComBanco){
		this.sentarAlcancarComBanco = sentarAlcancarComBanco;
	}
	
	public void setSentarAlcancar(String sentarAlcancar){
		try{
			this.sentarAlcancar = Double.parseDouble(sentarAlcancar);
		} catch (Exception e){
			try {
				this.sentarAlcancar = Integer.parseInt(sentarAlcancar);
			}catch (Exception a){
			this.sentarAlcancar = 0;
			}
		}
	}
	
	public void setNoveMinutos(boolean noveMinutos){
		this.noveMinutos = noveMinutos;
	}
	
	public void setSeisMinutos(String seisMinutos){
		try{
			this.seisMinutos = Double.parseDouble(seisMinutos);
		} catch (Exception e){
			try{
				this.seisMinutos = Integer.parseInt(seisMinutos);
			} catch (Exception a){		
			this.seisMinutos = 0;
			}
		}
	}
	
	public Element exportarTesteAptidaoSaudeXML(){
		Element testeXML = new Element("testeAptidaoSaude");

		if((getNumAbdominais() != null) && (!getNumAbdominais().isEmpty()) 
		&& (getNumAbdominais().length() != 0)){
			Element numAbdominaisXML = new Element("numAbdominais");
			numAbdominaisXML.setText(getNumAbdominais());
			testeXML.addContent(numAbdominaisXML);			
		}

		if((getImc() != null) && (!getImc().isEmpty()) 
		&& (getImc().length() != 0)){
			Element imcXML = new Element("imc");
			imcXML.setText(getImc());
			testeXML.addContent(imcXML);		
		}
		
		if( getNoveMinutos() ){
			Element noveMinutosXML = new Element("noveMinutos");
			noveMinutosXML.setText("true");
			testeXML.addContent(noveMinutosXML);
		}else{
			Element noveMinutosXML = new Element("noveMinutos");
			noveMinutosXML.setText("false");
			testeXML.addContent(noveMinutosXML);
		}
		
		if((getSeisMinutos() != null) && (!getSeisMinutos().isEmpty())
		&& (getSeisMinutos().length() != 0)){
			Element seisMinutosXML = new Element("seisMinutos");
			seisMinutosXML.setText(getSeisMinutos());
			testeXML.addContent(seisMinutosXML);
		}
		
		return testeXML;
	}
}
