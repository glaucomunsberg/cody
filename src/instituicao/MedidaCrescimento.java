package instituicao;

import org.jdom2.Element;

/** Classe responsável aos dados de Medidas fisicas de um aluno. Contém todas as operações para edição destes dados.
 * Um teste de medida de crescimento possui os dados referentes a massa corporal, estatura e envergadura de um aluno.
 * @author Equipe DSSkywalker.
 *
 */
public class MedidaCrescimento extends Teste {
	
	private double massaCorporal;
	private double estatura;
	private double envergadura;
	
	public MedidaCrescimento(){
		massaCorporal = 0;
		estatura = 0;
		envergadura = 0;
	}
	
	public MedidaCrescimento(String data, String nome){
		super(data, nome);
		massaCorporal = 0;
		estatura = 0;
		envergadura = 0;
	}
	
	public MedidaCrescimento(String data, String nome, String massaCorporal, String estatura, String envergadura){
		
		super(data, nome);
		
		try{
			this.massaCorporal = Double.parseDouble(massaCorporal);
		} catch (Exception e){
			this.massaCorporal = 0;
		}
		try{
			this.estatura = Double.parseDouble(estatura);
		} catch (Exception e){
			this.estatura = 0;
		}	
		try{
			this.envergadura = Double.parseDouble(envergadura);
		} catch (Exception e){
			this.envergadura = 0;
		}
	}
	
	public String getMassaCorporal(){
		return String.valueOf(massaCorporal);
	}
	
	public String getEstatura(){
		return String.valueOf(estatura);
	}
	
	public String getEnvergadura(){
		return String.valueOf(envergadura);
	}
	
	public void setMassaCorporal(String massaCorporal){
		try{
			this.massaCorporal = Double.parseDouble(massaCorporal);
		} catch (Exception e){
			try{
				this.massaCorporal = Integer.parseInt(massaCorporal);
			} catch (Exception a){
				this.massaCorporal = 0;
			}
		}
	}
	
	public void setEstatura(String estatura){
		try{
			this.estatura = Double.parseDouble(estatura);
		} catch (Exception e){
			try{
				this.estatura = Integer.parseInt(estatura);
			} catch (Exception a){
				this.estatura = 0;
			}
		}
	}
	
	public void setEnvergadura(String envergadura){
		try{
			this.envergadura = Double.parseDouble(envergadura);
		} catch (Exception e){
			try{
				this.envergadura = Integer.parseInt(envergadura);
			} catch (Exception a){
				this.envergadura = 0;
			}
		}
	}
	
	public Element exportarMedidaCrescimentoXML(){
		Element testeXML = new Element("medidaCrescimento");
		
		if((getMassaCorporal() != null) && (!getMassaCorporal().isEmpty()) 
		&& (getMassaCorporal().length() != 0)){
			Element massaCorporalXML = new Element("massaCorporal");
			massaCorporalXML.setText(getMassaCorporal());
			testeXML.addContent(massaCorporalXML);		
		}
			
		if((getEstatura() != null) && (!getEstatura().isEmpty()) 
		&& (getEstatura().length() != 0)){
			Element estaturaXML = new Element("estatura");
			estaturaXML.setText(getEstatura());
			testeXML.addContent(estaturaXML);		
		}
		
		if((getEnvergadura() != null) && (!getEnvergadura().isEmpty()) 
		&& (getEnvergadura().length() != 0)){
			Element envergaduraXML = new Element("envergadura");
			envergaduraXML.setText(getEnvergadura());
			testeXML.addContent(envergaduraXML);			
		}
		return testeXML;		
	}
}
