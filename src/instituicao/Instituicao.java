package instituicao;


import java.io.StringReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jdom2.Document;
import org.jdom2.Element;

import system.Cody;

/**Classe responsável pela descrição de uma instituicao de ensino especial ou regular, contém todos os dados referentes a descrição da instituicao
 * e as turmas ligadas a ela.
 * 
 * @author Equipe DSSkywalker
 *
 */
public class Instituicao {
	
	private String nome;
	private String cnpj;
	private String tipo;
	private ArrayList<Turma> turmas;

	
	public Instituicao(){
		turmas = new ArrayList<Turma>();
		
	}
	
	public Instituicao(String nome, String cnpj, String tipo){
		this.nome = nome;
		this.cnpj = cnpj;
		this.tipo = tipo;
		turmas = new ArrayList<Turma>();
	
	}
	
	public String getNome(){
		return this.nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
		
	}
	
	public String getCnpj(){
		return cnpj;
	}
	
	public void setCnpj(String cnpj){
		this.cnpj = cnpj;
		
	}
	
	public String getTipo(){
		return tipo;
	}
	
	public ArrayList<Turma> getListaTurmas(){
		return this.turmas;
	}
	
	public void setTipo(String tipo){
		this.tipo = tipo;
		
	}
	
	public String[] getTurmas(){
		String[] retornaTurmas = new String[ turmas.size() ];
		int a = 0;
		for(Turma turma : turmas){
			retornaTurmas[a] =turma.getNome() + " " +  turma.getAno();
			a++;
		}
		return retornaTurmas;
	}
	
	public Turma getTurma(int id){
		return turmas.get(id);
	}
	
	public void setTurmas(ArrayList<Turma> turmas){
		this.turmas.clear();
		for(Turma turma : turmas){
			this.turmas.add(turma);
		}
	}
	
	public void setNomeTurmas(ArrayList<Turma> turmas){
		this.turmas.clear();
		for(Turma turma : turmas){
			this.criarNovaTurma(turma.getNome(), turma.getAno());
		}
		
	}
	
	public void excluirTurma(String identificador){
		turmas.remove(Integer.parseInt(identificador));
		
	}
	
	public boolean criarNovaTurma(String nome, String ano){
		for(Turma turma: turmas){
			if(turma.getNome().equals(nome) && turma.getAno().equals(ano)){
				return false;
			}
		}
		turmas.add(new Turma(nome, ano));
		return true;
	}
	
	public int getNumeroDeTurmas() {
		return turmas.size();
	}
	
	public void validaCnpj(String cnpj){
		
	}
	
	public boolean setTurmasXML(List<Element> turmasRaiz){
		String nomeTurma, anoTurma;
		List<Element> alunosNaTurma = null;
		ArrayList<Turma> turmasDoXml = new ArrayList<Turma>();
		Turma instTurma;
		
		for(Element turmaTag: turmasRaiz){
			nomeTurma = turmaTag.getChildText("nome");
			anoTurma = turmaTag.getChildText("ano");
			
			if(nomeTurma != null){			
				instTurma = new Turma(nomeTurma, anoTurma);
		
				alunosNaTurma =  turmaTag.getChildren("aluno");
				
				instTurma.importarTurmaXml(alunosNaTurma);
	
				turmasDoXml.add(instTurma);
			}
		}
		setTurmas(turmasDoXml);
		return true;
	}
	
	public static String md5Java(Instituicao message){ 
		String digest = null; 
		try { 
				MessageDigest md = MessageDigest.getInstance("MD5"); 
				byte[] hash = md.digest(message.toString().getBytes());
				StringBuilder sb = new StringBuilder(2*hash.length); 
				for(byte b : hash){ 
					sb.append(String.format("%02x", b&0xff));
				} 
				digest = sb.toString(); 
			} catch (NoSuchAlgorithmException ex) { 
				Logger.getLogger(StringReader.class.getName()).log(Level.SEVERE, null, ex); 
			} 
			return digest;
	}
	
	public Document getInstituicaoXML(){
		int i;
		
		Element instXML = new Element("instituicao");
		Document doc = new Document(instXML);
		
		Element instNome = new Element("nome");
		instNome.setText(nome);
		
		Element instCnpj = new Element("cnpj");
		instCnpj.setText(formataString(cnpj));
		
		Element instTipo = new Element("tipo");
		instTipo.setText(tipo);
		
		instXML.addContent(instNome);
		instXML.addContent(instCnpj);
		instXML.addContent(instTipo);
		
		if(getNumeroDeTurmas() > 0){
			for(i=0;i<getNumeroDeTurmas();i++){
				Element turmaXML;
				turmaXML = turmas.get(i).exportarTurmaXml();
				instXML.addContent(turmaXML);
			}
		}
		
		Element instHash = new Element("hash");
		instHash.setText(md5Java(this));
		instXML.addContent(instHash);
		
		Element programaVersao = new Element("versao");
		programaVersao.setText(Cody.getInstancia().getVersaoDoSistema());
		instXML.addContent(programaVersao);
		
		return doc;		
	}
	
	private String formataString(String val){
		val = val.replace('.',' ');
		val = val.replace('-',' ');
        val = val.replace('/',' ');
        val = val.replaceAll(" ","");
        return val;
	}
	
}
