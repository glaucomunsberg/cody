package instituicao;

import config.Time;

/**Classe responsável por um  teste. Está contem todas os métodos de manipulação dos dados de um Teste de um aluno.
 * 
 * @author Equipe DSSkywalker.
 *
 */
public class Teste {
	
	private String data;
	private String nome;
	private Time dataCorrente;
	
	public Teste(){
		dataCorrente = new Time();
		this.data = dataCorrente.getDate();
	}
	
	public Teste(String data){
		this.data = data;
	}
	
	public Teste(String data, String nome){
		this.data = data;
		this.nome = nome;
	}
	
	public String getData(){
		return data;
	}
	
	public String getNome(){
		return nome;
	}
	
	public void setData(String data){
		this.data = data;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
}
