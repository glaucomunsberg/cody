package config;
import javax.swing.JOptionPane;
/**Essa é a classe responsável pelas configurações do sistema em geral note que ela é um "singleton" do tipo Lazy initialization e 
 * possui métodos que auxiliam em várias necessidades do sistema.
 * 
 * @author Equipe DSSKywalker.
 */
public class Config {
    
	private static volatile Config instance = null;
    private Log log;
    
    private final String baseURL;
    private final String baseLogURL;
    private final String baseIconeURL;
    private final int larguraDaTela;
    private final int alturaDaTela;

    private final java.awt.Toolkit kit;

    private Config(){
    	log = new Log(false);
    	log.openFile();

		this.kit = java.awt.Toolkit.getDefaultToolkit();
		java.awt.Dimension tamTela = kit.getScreenSize(); 
        this.baseURL = System.getProperty("user.dir");
        this.baseLogURL = this.baseURL + "/data/log/";
        this.baseIconeURL = this.baseURL + "/data/icones/";
        this.larguraDaTela = tamTela.width;
        this.alturaDaTela = tamTela.height;
    }
    
    /**
     * Retorna o singleton do tipo Lazy initialization
     */
    public static Config getInstancia() {
        if (instance == null) {
        	synchronized (Config.class){
        		if (instance == null) {
        			instance = new Config();
        		}
        	}
        }
        return instance;
    }
    public java.awt.Toolkit getKit(){
    	return this.kit;
    }
    
    
    public String getBaseURL(){
    	return this.baseURL;
    }
    
    public String getBaseLogURL(){
    	return this.baseLogURL;
    }
    
    public String getBaseIconesURL(){
    	return this.baseIconeURL;
    }
    
    public int getLarguraDaTela(){
    	return this.larguraDaTela;
    }
    
    public int getAlturaDaTela(){
    	return this.alturaDaTela;
    }
    
    public void setLogAtivo(boolean valor){
    	log.setLogAtivo(valor);
    }
    
    public boolean getLogAtivo(){
    	return log.getLogAtivo();
    }
    
    /**
     * Método que insere uma nova mensagem no log do sistema
     * @param mensagem		A mensagem a ser inserida.
     */
    public void setNovaMensagemNoLog(String mensagem){
    	log.addLog(mensagem);
    }
    
    /**
	 * Notifica o usuário e pede confirmação.
	 * @param notificacao 		A notificação a ser apresentada.
	 * @return 		0 - Sim, 1- Nao
	 */
	public int notificarUsuarioComRetorno(String notificacao){
		return JOptionPane.showOptionDialog(null, notificacao,"escolha",JOptionPane.YES_NO_OPTION,  
				JOptionPane.QUESTION_MESSAGE, null,  
                new String[] {"Sim", "Não"}, "Não");
	}
	
	/**
	 * Esse método será usado para o usuário saber que um erro ocorreu no programa dele.
	 * @param  notificao	A notificação a ser apresentada.
	 * @param  tipo 		O tipo de notificação { "erro", "avisar", "informar", null }
	 */
	public void notificaUsuario(String notificao, String tipo)
	{
		if(tipo == null){
			JOptionPane.showMessageDialog(null, notificao,"Notificação",JOptionPane.DEFAULT_OPTION);
		}else{
			if("erro".equals(tipo)){
				JOptionPane.showMessageDialog(null, notificao, "Erro!", JOptionPane.ERROR_MESSAGE);
			}else{
				if("avisar".equals(tipo)){
					JOptionPane.showMessageDialog(null, notificao, "Aviso", JOptionPane.WARNING_MESSAGE);
				}else{
					if("informar".equals(tipo)){
						JOptionPane.showMessageDialog(null, notificao, "Atenção", JOptionPane.INFORMATION_MESSAGE);
					}else{
						JOptionPane.showMessageDialog(null, notificao,"Notificação",JOptionPane.DEFAULT_OPTION);
					}
				}
			}
		}
	}
	
}
