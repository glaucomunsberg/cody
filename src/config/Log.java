package config;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.SecurityException;					
import java.util.FormatterClosedException;
import java.util.NoSuchElementException;

/**Classe responsável pelos logs do sistema.
 * 
 * @author Equipe DSSkywalker.
 *
 */
public class Log 
{
	private Config config;
	protected static boolean jaInicializou = false;
	private static Time hora = new Time();
	private static BufferedWriter saida;
	protected static boolean logAtivo = false;
	protected String enderecoDoArquivo;
	
	public Log()
	{
		config = Config.getInstancia();
		logAtivo = config.getLogAtivo();
	}
	
	public Log(boolean ativo)
	{
		logAtivo = ativo;
	}
	
	/**
	 * openFile abrirá o arquivou ou o criará para conter os logs do sistem
	 * @throws IOException 
	 */
	public void openFile()
	{
		if(jaInicializou && logAtivo)
		{
			this.addLog("Ooops! O log já foi inicializado.");
		}
		else
		{
			
			try
			{
				/**
				 * inicia o log se JAR então vai criar apenas o log no diretório de execução se for class então criará o log na pasta /config/Logs
				 */
				enderecoDoArquivo = String.format("%slog_%s-%s-%s.log",System.getProperty("user.dir")+"/bin/data/log/", hora.getDiaAtual(), hora.getMesAtual(),hora.getAnoAtual());			
				saida = new BufferedWriter(new FileWriter(enderecoDoArquivo, true));
				jaInicializou = true;
				
			}
			catch(IOException io)
			{
				System.err.printf("Atenção! Erro ao abrir o arquivo %s!",enderecoDoArquivo);
				System.err.printf("%s\n", io);
			}
			catch( SecurityException securityException)
			{
				System.err.println("Atenção! Você não tem permissão para abrir o arquivo de log!");
				System.err.printf("%s\n", securityException);
			}
			addLog("--------------------------------------------------------");
		}

	}
	
	/**
	 * Adiciona uma linha ao arquivo de log essa linha é constituída de: HH:MM:SS - "mensagem"
	 * @param novaLinha 	Mensagem que será gravada no log e evite usar o "\n"
	 */
	public void addLog(String novaLinha)
	{
		if( logAtivo)
		{
			hora = new Time();
			try
			{
				String temp = String.format("%s - %s\n", hora.getHoraAtual(), novaLinha);
				saida.write(temp);
				saida.flush();
			}

			catch( FormatterClosedException formatterClosedException)
			{
				System.err.println("Ooops! Nossos escribas encontraram um problema ao escrever o arquivo õ.o");
			}
			catch( NoSuchElementException element)
			{
				System.err.println("Ooops! Você mandou o tipo errado e ele não pode ser escrito. Lamentamos u.u");
				addLog("Ooops! Você mandou o tipo errado e ele não pode ser escrito. Lamentamos u.u");
				System.err.printf("%s\n", element);
			}
			catch( IOException io)
			{
				System.err.printf("Atenção! Erro ao tentar escrever: '%s'.", novaLinha);
				System.err.printf("%s\n", io);
			}
		}
	}

	/**
	 * Fecha o sistema de log para que não se perca nenhum dado de log
	 * @throws IOException 
	 */
	public void closeFile()
	{
		try
		{
			addLog("Fechando o sistema de log");
			if(saida != null)
			{
				saida.close();
			}
		}
		catch( IOException io)
		{
			addLog("Atenção! Não pode ser fechado o sistema de log do sistema.");
			System.err.println("Atenção! Não pode ser fechado o sistema de log do sistema.");
		}

	}
	
	/**
	 * @return		A Gravação do log está ativa
	 */
	public boolean getLogAtivo()
	{
		return logAtivo;
	}
	
	/**
	 * modifica a situacao de se o log está
	 * 	sendo gravado ou não
	 * @param novoEstado
	 */
	public void setLogAtivo(boolean novoEstado)
	{
		if(novoEstado == false)
		{
			addLog( "Log Desabilitado");
		}
		else
		{
			addLog("Log Habilitado");
		}
		logAtivo = novoEstado;
	}

}
