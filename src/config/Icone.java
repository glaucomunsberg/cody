/**
 * Esta classe contém todos os icones usados no sistema
 * @author Equipe DSSkywalker.
 */

package config;
import javax.swing.ImageIcon;
public class Icone
{	
	private Config config;
	private ImageIcon aluno;
	private ImageIcon alunos;
	private ImageIcon importar;
	private ImageIcon salvar;
	private ImageIcon salvarComo;
	private ImageIcon notas;
	private ImageIcon nota;
	private ImageIcon home;
	private ImageIcon teste;
	private ImageIcon testes;
	private ImageIcon turma;
	private ImageIcon turmas;
	private ImageIcon info;
	private ImageIcon instituicao;
	private ImageIcon ajuda;
	private ImageIcon retorno;
	private ImageIcon miniAbrir;
	private ImageIcon miniAjuda;
	private ImageIcon miniCriar;
	private ImageIcon miniHome;
	private ImageIcon miniInstituicao;
	private ImageIcon miniSair;
	private ImageIcon miniSalvar;
	private ImageIcon miniSalvaComo;
	private ImageIcon miniSobre;
	
	/**
	 * Inicia o sistema escolhendo o tema
	 * 1 - Carisma
	 * 2 - Metalico
	 */
	public Icone()
	{
		config	= Config.getInstancia();
		
		try{
			
			aluno			= new ImageIcon( Icone.class.getResource("/data/icones/oxygenAluno.png") );
			ajuda			= new ImageIcon( Icone.class.getResource("/data/icones/oxygenAjuda.png") );
			alunos			= new ImageIcon( Icone.class.getResource("/data/icones/oxygenAlunos.png") );
			importar		= new ImageIcon( Icone.class.getResource("/data/icones/oxygenArquivoImportar.png") );
			salvar 			= new ImageIcon( Icone.class.getResource("/data/icones/oxygenArquivoSalvar.png") );
			salvarComo		= new ImageIcon( Icone.class.getResource("/data/icones/oxygenArquivoSalvarComo.png") );
			notas 			= new ImageIcon( Icone.class.getResource("/data/icones/oxygenNotas.png") );
			nota 			= new ImageIcon( Icone.class.getResource("/data/icones/oxygenNota.png") );
			home 			= new ImageIcon( Icone.class.getResource("/data/icones/oxygenHome.png") );
			teste 			= new ImageIcon( Icone.class.getResource("/data/icones/oxygenTeste.png") );
			testes 			= new ImageIcon( Icone.class.getResource("/data/icones/oxygenTestes.png") );
			turma 			= new ImageIcon( Icone.class.getResource("/data/icones/oxygenTurma.png") );
			info			= new ImageIcon( Icone.class.getResource("/data/icones/oxygenInfo.png") );
			turmas 			= new ImageIcon( Icone.class.getResource("/data/icones/oxygenTurmas.png") );
			instituicao		= new ImageIcon( Icone.class.getResource("/data/icones/oxygenInstituicao.png") );
			miniAbrir		= new ImageIcon( Icone.class.getResource("/data/icones/oxygenMiniAbrir.png") );
			miniAjuda		= new ImageIcon( Icone.class.getResource("/data/icones/oxygenMiniAjuda.png") );
			miniCriar		= new ImageIcon( Icone.class.getResource("/data/icones/oxygenMiniCriar.png") );
			miniHome		= new ImageIcon( Icone.class.getResource("/data/icones/oxygenMiniHome.png") );
			miniInstituicao	= new ImageIcon( Icone.class.getResource("/data/icones/oxygenMiniInstituicao.png") );
			miniSair		= new ImageIcon( Icone.class.getResource("/data/icones/oxygenMiniSair.png") );
			miniSalvar		= new ImageIcon( Icone.class.getResource("/data/icones/oxygenMiniSalvar.png") );
			miniSalvaComo	= new ImageIcon( Icone.class.getResource("/data/icones/oxygenMiniSalvarComo.png") );
			miniSobre		= new ImageIcon( Icone.class.getResource("/data/icones/oxygenMiniSobre.png") );
		}finally{
			config.setNovaMensagemNoLog("Tema dos Icones: Oxygen");
		}
	}
	
	public ImageIcon getIcone(String icone){
		if("miniNovo".equals(icone)){
			retorno = miniCriar;
		}else{
			if("miniAbrir".equals(icone)){
				retorno = miniAbrir;
			}else{
				if("miniSalvar".equals(icone)){
					retorno = miniSalvar;
				}else{
					if("miniSalvarComo".equals(icone)){
						retorno = miniSalvaComo;
					}else{
						if("miniSair".equals(icone)){
							retorno = miniSair;
						}else{
							if("miniInicio".equals(icone)){
								retorno = miniHome;
							}else{
								if("miniInstituicao".equals(icone)){
									retorno = miniInstituicao;
								}else{
									if("miniComoUsar".equals(icone)){
										retorno = miniAjuda;
									}else{
										if("miniSobre".equals(icone)){
											retorno = miniSobre;
										}else{
											if("aluno".equals(icone)){
												retorno = aluno;
											}else{
												if("alunos".equals(icone)){
													retorno = alunos;
												}else{
													if("importar".equals(icone)){
														retorno = importar;
													}else{
														if("salvar".equals(icone)){
															retorno = salvar;
														}else{
															if("salvarComo".equals(icone)){
																retorno = salvarComo;
															}else{
																if("nota".equals(icone)){
																	retorno =nota;
																}else{
																	if("notas".equals(icone)){
																		retorno = notas;
																	}else{
																		if("home".equals(icone)){
																			retorno = home;
																		}else{
																			if("testes".equals(icone)){
																				retorno = testes;
																			}else{
																				if("teste".equals(icone)){
																					retorno = teste;
																				}else{
																					if("turmas".equals(icone)){
																						retorno = turmas;
																					}else{
																						if("turma".equals(icone)){
																							retorno = turma;
																						}else{
																							if("info".equals(icone)){
																								retorno = info;
																							}else{
																								if("instituicao".equals(icone)){
																									retorno = instituicao;
																								}else{
																									if("ajuda".equals(icone)){
																										retorno = ajuda;
																									}else{
																										retorno = home;
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return retorno;
	}

}
