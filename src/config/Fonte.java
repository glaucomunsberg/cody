/**Fonte responsável pela fonte do sistema inteiro,usado pelas nos JFrames. Possui a classe duas fontes: Ubuntu Serif
 * 
 * @author glaucoroberto@gmail.com
 * @project SSH: git@github.com:glaucomunsberg/Javeco.git
 * 
 */

package config;
import java.awt.Font;
public class Fonte 
{
	private Font fonteTitulo;
	private Font fonteTexto;
	private Font fonteTextoBold;
	private Config config;
	
	public Fonte()
	{
		fonteTitulo = new Font("Ubuntu", Font.BOLD, 18);
		fonteTexto = new Font("Ubuntu", Font.PLAIN, 14);
		fonteTextoBold = new Font("Ubuntu", Font.BOLD, 14);
		config = Config.getInstancia();
		config.setNovaMensagemNoLog("Tema da Fonte: Ubuntu");
	}
	
	
	/**
	 * retorna fonte para texto em geral
	 */
	public Font getFontTexto()
	{
		return fonteTexto;
	}
	
	/**
	 * retorna fonte para o titulo em geral
	 */
	public Font getFontTitulo()
	{
		return fonteTitulo;
	}
	
	/**
	 * retorna a fonte para o texto, porém com bold
	 */
	public Font getFontTextoBold()
	{
		return fonteTextoBold;
	}

}
